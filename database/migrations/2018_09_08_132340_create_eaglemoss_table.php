<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEaglemossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eaglemoss', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('descricao');
            $table->string('historia');
            $table->string('pdf');
            $table->string('local_de_nascimento');
            $table->string('codinomes');
            $table->string('filiacao');
            $table->string('foto');
            $table->string('peso');
            $table->string('altura');
            $table->string('olhos');
            $table->string('cabelos');
            $table->string('inteligencia');
            $table->string('forca');
            $table->string('velocidade');
            $table->string('durabilidade');
            $table->string('projecao_de_energia');
            $table->string('combate');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eaglemoss');
    }
}

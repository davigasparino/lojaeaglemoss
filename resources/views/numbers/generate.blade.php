<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Number</th>
        <th scope="col">Quantity</th>
        <th scope="col">%</th>
    </tr>
    </thead>
    <tbody>
    <?php $count = 0; ?>
    @foreach($arr as $key => $lc)
        <?php $count++; ?>
        <tr>
            <td class="text-black-50 text-secondary">{{ $count }}°</td>
            <td class="font-weight-bold">{{ $key }} </td>
            <td>{{ $lc }}</td>
            <td><?php echo number_format((($lc / $total) * 100), 2); ?>%</td>
        </tr>
    @endforeach
    </tbody>
</table>
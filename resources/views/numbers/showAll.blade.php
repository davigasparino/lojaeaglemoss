<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Edição</th>
        <th scope="col">N1</th>
        <th scope="col">N2</th>
        <th scope="col">N3</th>
        <th scope="col">N4</th>
        <th scope="col">N5</th>
        <th scope="col">N6</th>
        <th scope="col">Data</th>
        <th scope="col"><i class="fas fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach( $megasena as $row )
        <tr>
            <th data-id="{{$row->id}}"> @if(isset($row->con)){{ $row->con }}@endif </th>
            <td> {{ $row->n1 }} </td>
            <td> {{ $row->n2 }} </td>
            <td> {{ $row->n3 }} </td>
            <td> {{ $row->n4 }} </td>
            <td> {{ $row->n5 }} </td>
            <td> {{ $row->n6 }} </td>
            <td> {{ $row->data }} </td>
            <td>
                <button type="button" data-toggle="modal"
                        data-target="#dellNumbersModal" class="btn btn-light btn-number-delete" ><i class="far fa-trash-alt"></i></button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<nav aria-label="Page navigation">
    {{ $megasena->links() }}
</nav>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Number</th>
        <th scope="col">Quantity</th>
        <th scope="col">%</th>
    </tr>
    </thead>
    <tbody>
    @foreach($luckNumbers as $key => $lc)
        <tr>
            <td class="text-black-50 text-secondary">{{ $key+1 }}°</td>
            <td class="font-weight-bold">{{ $lc['number'] }} </td>
            <td>{{ $lc['qtd'] }}</td>
            <td><?php echo number_format((($lc['qtd'] / $total) * 100), 2); ?>%</td>
        </tr>
    @endforeach
    </tbody>
</table>
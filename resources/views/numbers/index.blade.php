@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="row col-12 pb-5">
            <div class="card content-luckNumbers col-4 p-0 mt-3 mb-3">
                <div class="card-header bg-transparent border-bottom-0">
                    <div class="row align-items-center ">
                        <div class="col-10 ">
                            <i class="fas fa-medal"></i> Luck Numbers
                        </div>
                        <div class="col-2 ">
                            <button id="luckNumbers" class="btn btn-light float-right">
                                <i class="fas fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body overflow-auto p-0">

                </div>
                <div class="card-footer bg-transparent p-0">

                </div>
            </div>

            <div class="card content-manyNumbers col-4 p-0 mt-3 mb-3">
                <div class="card-header bg-transparent border-bottom-0">
                    <div class="row align-items-center">
                        <div class="col-10 ">
                            <i class="far fa-gem"></i> Many simulations
                        </div>
                        <div class="col-2 ">
                            <button id="onemillion" class="btn btn-light">
                                <i class="fas fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body overflow-auto p-0">

                </div>
                <div class="card-footer bg-transparent p-0">

                </div>
            </div>

            <div class="card content-generateNumbers col-4 p-0 mt-3 mb-3">
                <div class="card-header bg-transparent border-bottom-0">
                    <div class="row align-items-center ">
                        <div class="col-4 ">
                            <i class="fas fa-dice-six"></i> Generate
                        </div>
                        <div class="col-6">
                            <div class="collapse multi-collapse p-0" id="simulator">
                                <form method="post" class="form-row col-12">
                                    <div class="col-10">
                                        <input type="number" class="form-control w-100" name="size">
                                    </div>
                                    <div class="col-2">
                                        <button id="generateCombination" type="button" class="btn btn-light btn-outline-secondary">
                                            <i class="fas fa-caret-right"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-2 ">
                            <button type="button" class="btn btn-light" data-toggle="collapse" data-target="#simulator" aria-expanded="false" aria-controls="simulator">
                                <i class="fas fa-sync-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body overflow-auto p-0">

                </div>
            </div>

            <div class="card col-6 p-0" id="allNumbersContent">
                <div class="card-header bg-transparent border-bottom-0">
                    <div class="row align-items-center">
                        <div class="col-5">
                            <i class="fas fa-list-ul"></i> All Numbers
                        </div>
                        <div class="col-7">
                            <button id="btInsertNumbers" type="button" class="btn btn-light float-right"
                                    data-toggle="collapse" data-target="#inserNumbers"
                                    aria-expanded="false" aria-controls="inserNumbers">
                                <i class="far fa-plus-square"></i>
                            </button>
                            <button id="allNumbers" type="button" class="btn btn-light float-right">
                                <i class="fas fa-sync-alt"></i>
                            </button>
                            <button id="btImportNumbers" type="button" class="btn btn-light float-right"
                                    data-toggle="collapse" data-target="#importNumbers"
                                    aria-expanded="false" aria-controls="importNumbers">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </button>
                            <a  href="/admin/numbers/export/excel" type="button" class="btn btn-light float-right">
                                <i class="fas fa-cloud-download-alt"></i>
                            </a>

                        </div>
                    </div>
                    <div class="collapse multi-collapse" id="importNumbers">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">

                            <form class="form-horizontal readCSV" action="/admin/numbers" method="post" name="uploadCSV"
                                  enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="col-11">
                                        <input
                                                type="file" name="file" id="file" accept=".csv">
                                    </div>
                                    <div class="col-1">
                                        <button type="submit" id="submitreadCSV" name="import"
                                                class="btn-submit btn btn-light btn-outline-secondary">
                                            <i class="fas fa-cloud-upload-alt"></i>
                                        </button>
                                    </div>
                                </div>
                                <div id="labelError"></div>
                            </form>

                        </nav>
                    </div>
                    <div class="collapse multi-collapse" id="inserNumbers">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <form method="post">
                                <div class="form-row">
                                    <div class="col-2">
                                        <input type="text" name="con" class="form-control" maxlength="6" data-id="" placeholder="Edição">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n1" class="form-control" maxlength="2" data-id="1" placeholder="n1">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n2" class="form-control" maxlength="2" data-id="2" placeholder="n2">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n3" class="form-control" maxlength="2" data-id="3" placeholder="n3">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n4" class="form-control" maxlength="2" data-id="4" placeholder="n4">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n5" class="form-control" maxlength="2" data-id="5" placeholder="n5">
                                    </div>
                                    <div class="col-1">
                                        <input type="text" name="n6" class="form-control" maxlength="2" data-id="6" placeholder="n6">
                                    </div>
                                    <div class="col-3">
                                        <input type="date" name="data" class="form-control">
                                    </div>
                                    <div class="col-1">
                                        <button type="button" class="btn btn-light btn-outline-secondary"><i class="far fa-plus-square"></i></button>
                                    </div>
                                </div>
                            </form>

                        </nav>
                    </div>
                </div>

                <div class="card-body p-0">

                </div>


            </div>

            <div class="card col-6 p-0" id="getCombinationContent">
                <div class="card-header bg-transparent border-bottom-0">
                    <div class="row align-items-center ">
                        <div class="col-2 ">
                            <i class="fas fa-filter"></i> Filter
                        </div>
                        <div class="col-8">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light collapse multi-collapse p-0" id="searchtAllNumbers">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <input type="text" name="n1" class="form-control" maxlength="2" data-id="1">
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="n2" class="form-control" maxlength="2" data-id="2">
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="n3" class="form-control" maxlength="2" data-id="3">
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="n4" class="form-control" maxlength="2" data-id="4">
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="n5" class="form-control" maxlength="2" data-id="5">
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="n6" class="form-control" maxlength="2" data-id="6">
                                        </div>
                                    </div>
                                </form>

                            </nav>
                        </div>


                        <div class="col-2 ">
                            <button id="getCombination" type="button" class="btn float-right"
                                    data-toggle="collapse" data-target="#searchtAllNumbers" aria-expanded="false"
                                    aria-controls="searchtAllNumbers">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="card-body overflow-auto p-0">

                    <div class="content-getallNumbers"></div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="dellNumbersModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir Personagem</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja mesmo remover permanentemente <span class="name"> </span> e todos seus registros?
                </div>
                <div class="modal-footer">
                    <form>
                        <input type="hidden" name="name" value="" />
                        <input type="hidden" name="id" value="" />
                        <button type="button" id="sairmodal" class="btn btn-secondary text-light" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger" id="removeNumber">Excluir</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

    <div class="panel panel-default">
        <div class="btn-group-vertical btn-block">
            <a class="btn btn-light text-left m-1 btn {{ (url()->current() == route('admin')) ? 'active' : '' }}"
               href="/admin"><i class="fas fa-home mr-2"></i> Home
            </a>
            <a class="btn btn-light text-left m-1 {{ (url()->current() == route('banner')) ? 'active' : '' }}"
               href="/admin/banner"><i class="fa fa-image mr-2"></i> Banner
            </a>
            <a class="btn btn-light text-left m-1  {{ (url()->current() == route('eaglemoss')) ? 'active' : '' }} "
               href="/admin/eaglemoss"><i class="fas fa-chess mr-2"></i> Eaglemoss
            </a>
            <a class="btn btn-light text-left m-1 {{ (url()->current() == route('numbers')) ? 'active' : '' }}"
               href="/admin/numbers"><i class="fas fa-chart-line mr-2"></i> Numbers
            </a>
            <a class="btn btn-light text-left m-1 disabled"
               href="/admin/products"><i class="fas fa-shopping-bag mr-2"></i> Products
            </a>
        </div>
    </div>


<div class="row ">
    <div class="col-md-10 center-block FormFilter">
        <form name="filter_eaglemoss" id="filter-eaglemoss" method="post" enctype="multipart/form-data" style="margin: 30px 0px;">
            <input type="hidden" name="search-filter" id="search-filter">
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Inteligência
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="inteligencia" id="filter-inteligencia">
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Força
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="forca" id="filter-forca">
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Velocidade
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="velocidade" id="filter-velocidade">
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Durabilidade
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="durabilidade" id="filter-durabilidade">
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Projeção de Energia
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="projecao_de_energia" id="filter-projecao_de_energia">
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="col-md-3">
                    Combate
                </div>
                <div class="col-md-1 scalPower">
                    <span></span>
                </div>
                <div class="form-group col-md-8 d-flex mb-0">
                    <input type="range" value="0" class="custom-range" min="0" max="7" name="combate" id="filter-combate">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-4 total-search"><span></span></div>
    <div class="col-md-4"></div>
    <div class="col-md-4 eaglemossPaginate">{{ $eaglemoss->links() }}</div>
</div>
<div class="row pl-3 pr-3  d-none d-md-flex">
    <div class="col-md-1">Id</div>
    <div class="col-md-3 ">Imagem</div>
    <div class="col-md-3 ">Descrição</div>
    <div class="col-md-4 ">Escala de poder</div>
    <div class="col-md-1 "><i class="fas fa-cogs"></i></div>
</div>
<div class="container-fluid body-all p-0" id="table-res" data-total="{{$eaglemoss->total()}}">


    @foreach($eaglemoss as $eg)
        <div class="row lineEagle p-0" id="tr-eagle-{{ $eg->id }}"
             data-name="{{ $eg->name }}"
             data-id="{{ $eg->id }}"
             data-codinomes="{{ $eg->codinomes }}"
             data-descricao="{{ $eg->descricao }}"
             data-historia="{{ $eg->historia }}"
             data-peso="{{ $eg->peso }}"
             data-preco="{{ $eg->preco }}"
             data-altura="{{ $eg->altura }}"
             data-olhos="{{ $eg->olhos }}"
             data-filiacao="{{ $eg->filiacao }}"
             data-local_de_nascimento="{{ $eg->local_de_nascimento }}"
             data-cabelos="{{ $eg->cabelos }}"
             data-inteligencia="{{ $eg->inteligencia }}"
             data-forca="{{ $eg->forca }}"
             data-velocidade="{{ $eg->velocidade }}"
             data-durabilidade="{{ $eg->durabilidade }}"
             data-pdf="{{ $eg->pdf }}"
             data-foto="{{ $eg->foto }}"
             data-combate="{{ $eg->combate }}"
             data-projecao_de_energia="{{ $eg->projecao_de_energia }}">
            <div class="col-1">
                {{$eg->id}}
            </div>
            <div class="col-md-3 col-11 text-center p-2">
                <a data-toggle="modal" data-foto="/uploads/{{ $eg->foto}}" data-target="#viewfoto" style="cursor: -webkit-zoom-in;">
                    <img
                            src="/uploads/{{ $eg->foto }}"
                            alt=""
                            style="max-width: 100%" />
                </a></div>
            <div class="col-md-3 col-6 p-2">
                <table>
                    <tbody>
                    <tr>
                        <td>{{ $eg->name }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if($eg->codinomes != "")
                                {{ $eg->codinomes }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>{{ $eg->filiacao }}</td>
                    </tr>
                    <tr>
                        <td>{{ $eg->local_de_nascimento }}</td>
                    </tr>
                    <tr>
                        <td>Olhos: {{ $eg->olhos }}</td>
                    </tr>
                    <tr>
                        <td>Altura: {{ $eg->altura }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 col-6 p-2">
                <table class="text-nowrap text-right">
                    <tbody>
                    <tr>
                        <td>
                            Inteligência:
                        </td>
                        <td class="scal-power" data-power="{{$eg->inteligencia}}"></td>
                    </tr>
                    <tr>
                        <td>
                            Força:
                        </td>
                        <td class="scal-power" data-power="{{$eg->forca}}"></td>
                    </tr>
                    <tr>
                        <td>
                            Velocidade:
                        </td>
                        <td class="scal-power" data-power="{{$eg->velocidade}}"></td>
                    </tr>
                    <tr>
                        <td>
                            Durabilidade:
                        </td>
                        <td class="scal-power" data-power="{{$eg->durabilidade}}"></td>
                    </tr>
                    <tr>
                        <td>
                            Proj. Energia:
                        </td>
                        <td class="scal-power" data-power="{{$eg->projecao_de_energia}}"></td>
                    </tr>
                    <tr>
                        <td>
                            Combate:
                        </td>
                        <td class="scal-power" data-power="{{$eg->combate}}"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1 col-12 text-center p-2">
                <button type="button"
                        class="btn btn-light"
                        data-toggle="modal"
                        data-target="#Edit-Eaglemoss"><i class="far fa-edit"></i>
                </button>
                <button
                        data-toggle="modal"
                        data-target="#dellEaglemoss"
                        class="btn btn-light ">
                    <i class="far fa-trash-alt"></i>
                </button>
                <button type="button"
                        class="btn btn-light eaglemossComparationBt">
                    <i class="fab fa-wpforms"></i>
                </button>
            </div>
        </div>
    @endforeach
</div>
<div class="eaglemossPaginate">
    {{ $eaglemoss->links() }}
</div>


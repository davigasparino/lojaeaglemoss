<form name="form_eaglemoss" id="form-eaglemoss" method="post" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputname">Nome Completo</label>
            <input type="text" class="form-control" name="name" id="inputname">
            <input type="hidden" class="form-control" name="id" id="inputid">
        </div>
        <div class="form-group col-md-6">
            <label for="inputcodinomes">Codinomes</label>
            <input type="text" class="form-control" name="codinomes" id="inputocodinomes">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="local_de_nascimento">Local de Nascimento</label>
            <input type="text" class="form-control" name="local_de_nascimento" id="local_de_nascimento">
        </div>
        <div class="form-group col-md-6">
            <label for="inputfiliacao">Filiação</label>
            <input type="text" class="form-control" name="filiacao" id="inputfiliacao">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputdescricao">Descrição</label>
            <textarea class="form-control" id="inputdescricao" name="descricao" rows="5"></textarea>
        </div>
        <div class="form-group col-md-6">
            <label for="inputhistoria">História</label>
            <textarea class="form-control" id="inputhistoria" name="historia" rows="5"></textarea>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="inputpeso">Peso</label>
            <input type="text" class="form-control" name="peso" id="inputpeso">
        </div>
        <div class="form-group col-md-3">
            <label for="inputaltura">Altura</label>
            <input type="text" class="form-control" name="altura" id="inputaltura">
        </div>
        <div class="form-group col-md-3">
            <label for="inputpeso">Olhos</label>
            <select class="custom-select" name="olhos" id="inputolhos">
                <option selected>Escolha...</option>
                <option value="amarelos">Amarelos</option>
                <option value="acinzentados">Acinzentados</option>
                <option value="azuis">Azuis</option>
                <option value="brancos">Brancos</option>
                <option value="castanhos">Castanhos</option>
                <option value="variavel">Variável</option>
                <option value="verdes">Verdes</option>
                <option value="vermelhos">Vermelhos</option>
                <option value="violetas">Violetas</option>
                <option value="pretos">Pretos</option>
            </select>


        </div>
        <div class="form-group col-md-3">
            <label for="inputcabelos">Cabelos</label>
            <select class="custom-select" name="cabelos" id="inputcabelos">
                <option value="nenhum" selected>Nenhum</option>
                <option value="azuis">Azuis</option>
                <option value="brancos">Brancos</option>
                <option value="castanhos">Castanhos</option>
                <option value="castanho claro">Castanho Claro</option>
                <option value="castanho escuro">Castanho Escuro</option>
                <option value="indigo">Índigo</option>
                <option value="loiros">Loiros</option>
                <option value="platinados">Platinados</option>
                <option value="ruivos">Ruivos</option>
                <option value="roxos">Roxos</option>
                <option value="variavel">Variável</option>
                <option value="verdes">Verdes</option>
                <option value="vermelhos">Vermelhos</option>
                <option value="pretos">Pretos</option>
            </select>
        </div>
    </div>

    <input type="hidden" name="search-filter" id="search-filter">
    <div class="form-row mb-2">
        <div class="col-md-3">
            Inteligência
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="inteligencia" id="inteligencia">
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            Força
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="forca" id="forca">
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            Velocidade
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="velocidade" id="velocidade">
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            Durabilidade
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="durabilidade" id="durabilidade">
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            Projeção de Energia
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="projecao_de_energia" id="projecao_de_energia">
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            Combate
        </div>
        <div class="col-md-1 scalPower">
            <span></span>
        </div>
        <div class="form-group col-md-8 d-flex mb-0">
            <input type="range" value="0" class="custom-range" min="0" max="7" name="combate" id="combate">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <div id="add-picture-content" class="col-md-12" >
            </div>
        </div>
        <div class="form-group col-md-6">
            <div id="add-pdf-content" >
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <input type="file" class="custom-file-input col-md-12" name="foto" id="fotoup" lang="pt"><label class="custom-file-label" for="fotoup">Selecionar foto</label>
        </div>
        <div class="form-group col-md-6">

            <input type="file" class="custom-file-input" name="pdf" id="pdfup" lang="pt"><label class="custom-file-label" for="fotoup">Selecionar pdf</label>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="preco">Preço</label>
            R$ <input type="text" class="form-control" name="preco" id="preco">
        </div>

    </div>


    <button class="btn btn-secondary text-light" id="btn-add-eaglemoss">Salvar</button>
</form>
@extends('layouts.app')

@section('content')
<div class="col-md-10 bg-light">
    <div class="panel panel-default">
        <nav class="navbar navbar-expand-lg navbar-light ">
            <span class="navbar-brand mb-0 ">Eaglemoss</span>

            <div class="collapse navbar-collapse mainTopBody">
                <ul class="navbar-nav float-sm-right">
                    <li class="nav-item active">
                        <a class="nav-link" data-target="#Add-Eaglemoss" data-toggle="modal">
                            <i class="far fa-plus-square"></i>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link FilterBT">
                            <i class="fas fa-sliders-h"></i>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link">
                            <i class="fas fa-sitemap"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </nav>
        @include('eaglemoss/comparation')

        @include('eaglemoss/filter')

        <div class="container-fluid badge-light">
            <div class="eagletable">
                {!! view('eaglemoss/eaglemoss', compact('eaglemoss')) !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-eaglemoss-modal-lg" id="Edit-Eaglemoss" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Edição de Personagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('eaglemoss/form')
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-eaglemoss-modal-lg" id="Add-Eaglemoss" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Criação de Personagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('eaglemoss/form')
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewfoto">
    <div class="modal-dialog">
        <div class="modal-content">
            {{--<img src="{{ \Illuminate\Support\Facades\Storage::url($eg->foto) }}" style="width: 100%;" alt="" />--}}
        </div>
    </div>
</div>

<div class="modal fade" id="dellEaglemoss" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Excluir Personagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja mesmo remover permanentemente <span class="name"> </span> e todos seus registros?
            </div>
            <div class="modal-footer">
                <form>
                    <input type="hidden" name="name" value="" />
                    <input type="hidden" name="id" value="" />
                    <button type="button" id="sairmodal" class="btn btn-secondary text-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" id="removeeagle">Excluir</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
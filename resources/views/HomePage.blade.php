@extends('layouts.homeIndex')

@section('content')

            <div id="carouselHomePage" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner">

                    @foreach( $banner as $bn )
                        <div class="carousel-item">
                            <img src="uploads/{{ $bn->banner }}" class="img-fluid d-block" alt="marvel">
                        </div>
                    @endforeach

                </div>
                <a class="carousel-control-prev" href="#carouselHomePage" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselHomePage" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <ol class="carousel-indicators">
                    @for($i = 0; $i<$banner->total(); $i++)
                        <li data-target="#carouselHomePage" data-slide-to="{{$i}}"></li>
                    @endfor
                </ol>
            </div>


    <div class="container">
        {{ session('Athena') }} <br />
        <?php
            echo session_status();
        ?>

        <div class="row" id="homeProducts">
            @foreach($eaglemoss as $eagle)
                <div class="col-md-3 col-sm-12 products" id="product-{{ $eagle->id }}" data-id="{{ $eagle->id }}" data-preco="{{ $eagle->preco }}">
                    <div class="container-img">
                        <img src="uploads/{{ $eagle->foto }}" alt="{{ $eagle->name }}">
                    </div>
                    <div class="container-desc">
                        <h3>{{ $eagle->name }}</h3>

                        <button href="/" class="btn btn-danger btn-block">
                             <input type="number" min="1" value="1"> <i class="fas fa-cart-plus"></i> R$ {{ $eagle->preco }}
                        </button>
                    </div>
                </div>
            @endforeach

            {{ $eaglemoss->links() }}

        </div>

    </div>
@endsection
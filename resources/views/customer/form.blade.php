<form id="form-customer" class="form-horizontal">

            <input id="lastname" type="hidden" class="form-control" name="lastname" value="{{ Auth::user()->lastname }}">
            <input id="name" type="hidden" class="form-control" name="name" value="{{ Auth::user()->name }}">
            <input id="user_id" type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">

    <div class="form-group">
        <label for="rg" class="col-md-4 control-label">RG</label>
        <div class="col-md-6">
            <input id="rg" type="text" class="form-control" name="rg" value="">
        </div>
    </div>
    <div class="form-group">
        <label for="cpf" class="col-md-4 control-label">CPF</label>
        <div class="col-md-6">
            <input id="cpf" type="text" class="form-control" name="cpf" value="">
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-md-4 control-label">Address</label>
        <div class="col-md-6">
            <input id="address" type="text" class="form-control" name="address" value="">
        </div>
    </div>
    <div class="form-group">
        <label for="complement" class="col-md-4 control-label">Complement</label>
        <div class="col-md-6">
            <input id="complement" type="text" class="form-control" name="complement" value="">
        </div>
    </div>
    <div class="form-group">
        <label for="telefone" class="col-md-4 control-label">Phone</label>
        <div class="col-md-6">
            <input id="telefone" type="text" class="form-control" name="telefone" value="">
        </div>
    </div>
    <div class="form-group">
        <label for="celular" class="col-md-4 control-label">Cellphone</label>
        <div class="col-md-6">
            <input id="celular" type="text" class="form-control" name="celular" value="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button id="btn-customer" class="btn btn-primary">
                Cadastrar
            </button>
        </div>
    </div>
</form>
<form name="form_banner" id="form-banner" method="post" enctype="multipart/form-data">

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>Banner</label>
            <div class="img-banner">

            </div>
            <div id="addbanner" class="custom-file">
                <input type="file" class="custom-file-input" name="banner" id="banner" lang="pt"><label class="custom-file-label" for="banner">Selecionar foto</label>
            </div>
        </div>

        <div class="form-group col-md-6">
            <label for="inputlink">Link</label>
            <input type="text" class="form-control" name="link" id="inputlink">
            <input type="hidden" class="form-control" name="id" id="inputid">
        </div>
    </div>


    <div class="form-row">
        <div class="form-group col-md-6">
            <label>Banner</label>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary ">
                    <input type="radio" name="active" value="1" id="active-on" autocomplete="off"> On
                </label>
                <label class="btn btn-secondary active">
                    <input type="radio" name="active" value="0" id="active-off" autocomplete="off" checked> Off
                </label>
            </div>
        </div>

        <div class="form-group col-md-6">
                <label>Target Blank</label>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-secondary ">
                        <input type="radio" name="target" id="blank-on" value="1" autocomplete="off"> On
                    </label>
                    <label class="btn btn-secondary active">
                        <input type="radio" name="target" id="blank-off" value="0" autocomplete="off" checked> Off
                    </label>
                </div>
            </div>
    </div>

    <button class="btn btn-secondary text-light" id="btn-add-eaglemoss">Salvar</button>
</form>
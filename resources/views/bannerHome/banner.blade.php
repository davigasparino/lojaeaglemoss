@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="panel panel-default">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <span class="navbar-brand mb-0 p">Banner</span>

                <div class="collapse navbar-collapse mainTopBody">
                    <ul class="navbar-nav float-sm-right">
                        <li class="nav-item active">
                            <a class="nav-link" data-target="#Add-Banner" data-toggle="modal">
                                <i class="far fa-plus-square"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3"><i class="far fa-image"></i></div>
                    <div class="col-sm-3"><i class="fas fa-link"></i></div>
                    <div class="col-sm-2"><i class="fas fa-external-link-alt"></i></div>
                    <div class="col-sm-2"><i class="fas fa-check"></i></div>
                    <div class="col-sm-2"><i class="fas fa-cogs"></i></div>
                </div>
            </div>
            <div class="container-fluid ListResultBanenr">
                @foreach($banner as $bn)
                    <div class="row">
                        <div class="listBanner" id="listBannerID{{ $bn->id }}" data-banner="{{ $bn->banner }}" data-target="{{ $bn->target }}" data-active="{{ $bn->active }}" data-link="{{ $bn->link }}" data-id="{{ $bn->id }}">
                            <div class="col-sm-3">
                                <img src="/uploads/{{ $bn->banner }}" alt="" style="max-width: 100%"/>
                            </div>
                            <div class="col-sm-3">
                                {{ $bn->link }}
                            </div>
                            <div class="col-sm-2">
                                @if($bn->target == 1)
                                    Nova aba
                                @else
                                    Mesma Aba
                                @endif
                            </div>
                            <div class="col-sm-2">
                                @if($bn->active == 1)
                                    Ativo
                                @else
                                    Inativo
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <button type="button"
                                        class="btn btn-light"
                                        data-toggle="modal"
                                        data-target="#Edit-Banner"><i class="far fa-edit"></i>
                                </button>
                                <button
                                        data-toggle="modal"
                                        data-target="#dellBanner"
                                        class="btn btn-light ">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    <div class="modal fade bd-eaglemoss-modal-lg" id="Add-Banner" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Criação de Banner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('bannerHome/form')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-eaglemoss-modal-lg" id="Edit-Banner" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Alteração de Banner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('bannerHome/form')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="dellBanner" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir Banner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja mesmo remover permanentemente este registro?
                </div>
                <div class="modal-footer">
                    <form>
                        <input type="hidden" name="id" value="" />
                        <button type="button" id="sairmodal" class="btn btn-secondary text-light" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger" id="removebanner">Excluir</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
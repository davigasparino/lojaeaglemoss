-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 30, 2018 at 11:15 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `LojaLaravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sr. Administrador', 'adm@adm.com', '$2y$10$xKbjKYjNnzwPmh078LynQuthhEomjxmn0I1Z16en7GfmZCkJfjelG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `complement`, `telefone`, `celular`, `rg`, `cpf`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'apalaches 929', 'casa 2', '(11) 2356-5656', '(11) 9.7417-8498', '40.631.640-5', '378.387.868-35', 1, '2018-09-08 17:20:39', '2018-09-08 17:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `eaglemoss`
--

CREATE TABLE `eaglemoss` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `historia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_de_nascimento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codinomes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filiacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peso` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `altura` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `olhos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cabelos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inteligencia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forca` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `velocidade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durabilidade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `projecao_de_energia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `combate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eaglemoss`
--

INSERT INTO `eaglemoss` (`id`, `name`, `descricao`, `historia`, `pdf`, `local_de_nascimento`, `codinomes`, `filiacao`, `foto`, `peso`, `altura`, `olhos`, `cabelos`, `inteligencia`, `forca`, `velocidade`, `durabilidade`, `projecao_de_energia`, `combate`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'James Howlett', 'Logan', 'Imortal', 'public/zCyZ1W8eh5V5rxsmqgPqD6U866FdCpgI5DfxkhNx.jpeg', 'Alberta, Canadá', 'Wolverine, Logan, Carcaju, Arma-X, Morte entre outros', 'Vingadores, X-Men', 'public/HNz8V9JQsWPm80BbZR6E6qZ1S7aq5B6aiW9pfjNO.jpeg', '75', '1.60', 'azuis', 'pretos', '2', '4', '2', '4', '1', '7', NULL, '2018-09-10 05:12:41', '2018-10-01 04:09:56'),
(2, 'Frank Castle', NULL, NULL, NULL, 'Queens, Nova York', 'Punisher', 'Fuzileiros Navais, Boinas-verdes', 'public/TcVkFu7SlwjYhju1l81Knk56Pe5E1g49b1auxgTK.jpeg', NULL, NULL, NULL, NULL, '1', '2', '2', '2', '1', '7', NULL, '2018-09-10 05:36:29', '2018-09-28 06:26:07'),
(3, 'Petter Parker', NULL, NULL, NULL, 'Nova York', 'Homen Aranha', 'Vingadores', 'public/Mn8fjleDtn2o7OI5L9tjfEC2FEzAFBSbU8VVLvOi.jpeg', '75', '1,78', 'castanhos claros', 'castanhos', '1', '4', '3', '3', '4', '5', NULL, '2018-09-10 06:17:32', '2018-09-21 06:44:56'),
(4, 'Thor Odinson', NULL, NULL, NULL, NULL, NULL, 'Vingadores, deuses de Asgard', 'public/t3hYkS6oZqPQmC0sPEjsI5DibPXC6pQO2KlRaDAi.jpeg', '290', '2', 'azuis', 'louro', '2', '7', '7', '6', '6', '4', NULL, '2018-09-11 02:36:25', '2018-09-21 06:48:23'),
(5, 'Anthony Stark', 'null', 'null', NULL, 'null', 'Homem de Ferro', 'Vingadores, Iluminati', 'public/kOt8NPj07SofpCLa6TP159D1arv9T9fohkoqpnNF.jpeg', '102', '1.85', 'azuis', 'pretos', '6', '6', '5', '6', '6', '3', NULL, '2018-09-11 02:40:38', '2018-09-21 06:48:39'),
(6, 'Norrin Radd', NULL, NULL, NULL, NULL, 'Surfista Prateado', 'Defensores', 'public/Rs7vlOEaQsXO6z0Lu1im0SzguWyDiizkmjbocygu.jpeg', '115', '1.93', 'prateads', 'nenhum', '3', '7', '7', '6', '7', '2', NULL, '2018-09-11 02:45:22', '2018-09-21 06:49:00'),
(7, 'Thanos', '7', '9', NULL, 'r', '8', 'nenhum', 'public/Qq5TH9t14De4jbZpjTS03em30AODY7pWzmUpqAUn.png', '447', '2.30', 'vermelhos', 'nenhum', '6', '7', '7', '7', '6', '4', NULL, '2018-09-11 02:48:24', '2018-09-21 06:50:53'),
(8, 'Jennifer Walters', NULL, NULL, NULL, NULL, 'Mulher Hulk', 'Vingadores, Heróis de aluguel', 'public/0kBxYjdd4ChOryexrshjiJxEBxSyLxvCJmXqq8H1.jpeg', '290', '2.05', 'verdes', 'verdes', '3', '6', '2', '5', '1', '3', NULL, '2018-09-11 03:18:19', '2018-09-21 06:51:09'),
(9, 'Jean Grey-Summers', NULL, NULL, NULL, NULL, 'Fenix', 'X-Men, X-Factor', 'public/alP9bOvpzsbjd1MHRJulLdMPDNavMReFMWmip9ei.jpeg', '52', '1.67', 'verdes', 'ruivos', '3', '2', '3', '2', '5', '3', NULL, '2018-09-11 04:03:38', '2018-09-21 06:52:14'),
(10, 'Steven Rogers 2', '5', '6', NULL, NULL, 'Capitão América', 'Vingadores, SHIELD, Invasores, Esquadrão Vitorioso', 'public/oaJrmA3faMSkQXXSX0ts4f8V9UGahSx8lOt6mBcA.jpeg', '100', '1.88', 'azuis', 'loiros', '3', '3', '2', '3', '1', '7', NULL, '2018-09-11 06:59:06', '2018-09-21 06:52:26'),
(11, 'Johanm Schmidt', 'Ex. Mendigo', 'Comandante Terrorista', NULL, 'Desconhecido', 'Caveira Vermelha', 'HIDRA, Terceiro Reich, IMA', 'public/WI2tTvXIjOQRyLo7MO8v2ieTadbueMC4J1up83C0.jpeg', '89', '1.86', 'azuis', 'nenhum', '5', '3', '2', '3', '1', '6', NULL, '2018-09-11 07:31:56', '2018-09-21 06:53:40'),
(12, 'Anna Marie', 'Aventureira, ex-mecânica, garçonete, terrorista.', 'CIdadã dos Estados Unidos', NULL, 'Nova York', 'Vampira', 'X-Men, ESX', 'public/S2REcNDzbFkZEFcuW3PYghQrDvK8JqQFRiA92JMm.jpeg', '55', '1.55', 'verdes', 'castanhos ocm uma mecha branca', '2', '2', '4', '2', '6', '4', NULL, '2018-09-11 07:37:37', '2018-09-21 06:54:00'),
(23, 'Max Eisenhardt', NULL, NULL, NULL, 'Alemanha', 'Magneto', 'Irmandade de Mutantes, Novos Mutantes, X-men, Acólitos', 'public/Rz1M4djkPhoiWwYD7dP16Dpya61q5JlWcfpZYzED.jpeg', '86', '1,87', 'azuis', 'brancos', '5', '2', '5', '6', '5', '3', NULL, '2018-09-16 23:29:26', '2018-09-21 06:55:01'),
(24, 'Ororo Munroe', NULL, NULL, NULL, 'Manhattan', 'Tempestade', 'X-Men, ESX', 'public/O6IvKkCEQcUiVxPO3Xw6Gr4YA3LadKkQaRTI5SOl.jpeg', '72', '1,80', 'azuis', 'brancos', '2', '2', '3', '2', '5', '3', NULL, '2018-09-16 23:37:30', '2018-09-21 06:55:15'),
(25, 'Henry Peter McCoy', NULL, NULL, NULL, 'Dunfee Illinois', 'Fera', 'X-men, Vingadores, X-Factor, E.S.P.A.D.A', 'public/WRyGuDtcLC7WT5IEY3MuKeTGe90SSj1ByOAQPYMN.jpeg', '161', '1,80', 'azuis', 'pretos', '3', '3', '2', '3', '1', '7', NULL, '2018-09-17 02:35:08', '2018-09-21 06:55:27'),
(26, 'Warren Kenneth Worthington III', NULL, NULL, NULL, 'Long Island, NY', 'Anjo', 'X-Men, X-Factory, Defensores, Campeões, Cavaleiros do Apocalipse', 'public/h9FTS83M190vxSvet3zvc5XkkzL40pynsKgM1866.jpeg', '68', '1,82', 'azuis', 'loiros', '3', '3', '3', '4', '1', '3', NULL, '2018-09-17 02:56:41', '2018-09-21 06:55:37'),
(105, 'Otto Octavius', NULL, NULL, NULL, 'Nova York', 'Dr. Octópus', 'Sexteto Sinistro', 'public/fEIl9rJWPWEYFTwC6VKK9e1VZ3mC3R62dkSeKvxg.jpeg', '111', '1,75', 'castanhos', 'castanhos', '7', '3', '4', '3', '2', '6', NULL, '2018-09-21 07:02:58', '2018-09-21 07:02:58'),
(106, 'Benjamin Jacob Grimm', NULL, NULL, NULL, 'Nova York', 'Coisa', 'Quarteto Fantástico', 'public/l7cpk34IQRAyEHWOAokV2esllgcYmIeN2sDRl5IK.jpeg', '227', '1,82', 'azuis', 'nenhum', '2', '6', '2', '6', '1', '5', NULL, '2018-09-21 07:11:49', '2018-09-21 07:11:49'),
(108, 'Eric Brooks', NULL, NULL, NULL, 'Não revelado', 'Blade', 'Filhos da meia noite, Caçadores da noite', 'public/gpwU8rn9i5BjXw2D28ZEIB0JvZ0OGbN2WVGnoPru.jpeg', '82', '1,88', 'castanhos', 'preto', '2', '3', '2', '4', '1', '5', NULL, '2018-09-21 07:20:35', '2018-09-21 07:21:07'),
(109, 'Norman Osbourn', NULL, NULL, NULL, 'Connecticut', 'Duende Verde', 'Orden dos duendes, Os doze Sinistros', 'public/M6c4kQIAqmmO053rrZkA0RBDynw4XO6l2lQaidDG.jpeg', '84', '1,80', 'azuis', 'Castanho escuro', '4', '4', '3', '4', '3', '3', NULL, '2018-09-22 22:21:42', '2018-09-22 22:21:42'),
(110, 'Victor Von Doom', NULL, NULL, NULL, 'Acampamento cigano aos redores de Haasenstadt, Latvéria.', 'Dr. Destino', 'Cabala de Norman Osbourn', 'public/RGkzfIFWPHQRgO2IgbmEpdKsy9KmtqSQZgQqncOC.jpeg', '102', '1.88', 'castanhos', 'castanhos', '6', '4', '5', '6', '6', '4', NULL, '2018-09-22 22:44:19', '2018-09-22 22:44:19'),
(111, 'Matthew Murdock', 'null', 'null', NULL, 'Nova York', 'Demolidor', 'ex membro Paladinos Marvel', 'public/ioCr3BOEYgze5puazKWrd9hKIIOd4RziPtbb9nQU.jpeg', '90', '1,84', 'azuis', 'ruivos', '3', '2', '2', '2', '1', '5', NULL, '2018-09-22 22:59:10', '2018-09-30 17:28:53'),
(112, 'Elektra Natchios', NULL, NULL, NULL, 'ilha perto de mar ereu', NULL, 'Tentáculo', 'public/4hH2YXUiB6g6ychfY5fK2TBd37bBCs54ulZt8Sw4.jpeg', '58', '1,80', 'azuis', 'pretos', '3', '2', '2', '3', '1', '6', NULL, '2018-09-22 23:07:39', '2018-09-22 23:07:39'),
(113, 'Jonathan Storm', NULL, NULL, NULL, 'Nova York', 'Tocha Humana', 'Quarteto Fantástico', 'public/4RBDUQOANhyZwIXXWzMMiUbMs11DikusRg8QeuHL.jpeg', '77', '1,77', 'azuis', 'loiros', '2', '2', '5', '2', '5', '3', NULL, '2018-09-22 23:17:20', '2018-09-22 23:17:20'),
(114, 'Felicia Hardy', NULL, NULL, NULL, 'Nova York', 'Gata Negra', 'nenhum', 'public/7IBqMqNpxfSZl2adVRqHabDN676Qhf5BNnQiKA1d.jpeg', '55', '1.70', 'verdes', 'platinados', '2', '4', '3', '2', '1', '3', NULL, '2018-09-22 23:20:17', '2018-09-22 23:20:17'),
(115, 'Brian Braddock', NULL, NULL, NULL, 'Nalden, Inglaterra', 'Capitão Britânia', 'Excalibur, Tropa do capitão britânia', 'public/YCN8uW2V4Xay1qQI7YyZwLqmf5VVntj80Q865hnN.png', '116', '1,82', 'azuis', 'loiros', '5', '6', '4', '6', '3', '4', NULL, '2018-09-22 23:33:16', '2018-09-22 23:33:16'),
(116, 'John Blaze', NULL, NULL, NULL, 'Waukegan, ilhinois', 'Motoqueiro Fantásma', 'Filhos da meia noite, Legião dos mosntros', 'public/nEBELarJgDAwqD4nIebH4egnixer5x8PAjGHORYB.jpeg', '100', '1,75', 'azuis', 'loiros', '2', '4', '4', '5', '4', '1', NULL, '2018-09-23 00:21:26', '2018-09-23 00:21:26'),
(119, 'Sergei Kravinoff', NULL, NULL, NULL, 'Rússia', 'Kraven o caçador', 'Sexteto Sinistro', 'public/6BnETHJBULDi7VwDL9sdGu1CDFh54NHifm0ZJkqV.jpeg', '109', '1,91', 'castanhos', 'pretos', '2', '3', '2', '4', '1', '5', NULL, '2018-09-23 00:29:17', '2018-09-23 00:29:17'),
(122, 'Mefisto', NULL, NULL, NULL, 'desconhecido', NULL, 'Senhores do Inferno', 'public/3jtf3ZFrG73sxTHWAlB2JtMsYGsiWp3JgXoP9KNZ.jpeg', 'variável', 'variável', 'variavel', 'variavel', '7', '7', '7', '7', '7', '2', NULL, '2018-09-23 00:36:11', '2018-09-23 00:39:02'),
(123, 'Scott Summers', NULL, NULL, NULL, 'Anchorage, Alasca', 'Ciclop', 'X-men', 'public/nMEnqaW3QVGklYR6KXB05hn7ZsgoY5084yaXmuKW.jpeg', '82', '1,86', 'castanhos', 'castanhos', '3', '2', '2', '2', '5', '4', NULL, '2018-09-23 00:48:31', '2018-09-23 00:48:31'),
(124, 'Ultron', NULL, NULL, NULL, 'Cresskill, New Jersey', NULL, NULL, 'public/UaVdsQtHez4vSAqJgxUfi6K3mAMySA1UMhKtkiDg.jpeg', '240', '1,85', 'verdes', 'nenhum', '4', '6', '2', '7', '6', '4', NULL, '2018-09-23 01:09:42', '2018-09-23 01:09:42'),
(125, 'William Baker', NULL, NULL, NULL, 'Queens, Nova York', 'Homem de Areia', 'Sexteto Sinistro, Quarteto Terrível', 'public/nCqKnGR2pUh8xsyHDzEL2XinWm2WnuYUINQqFzDW.png', '110', '1,85', 'castanhos', 'castanhos', '2', '6', '2', '6', '1', '2', NULL, '2018-09-23 01:15:49', '2018-09-23 01:15:49'),
(126, 'Reed Richards', NULL, NULL, NULL, 'Califórnia', 'Sr. Fantástico', 'Quarteto Fantástico', 'public/v7CT1WUgnKWT0UJu9X9ju9NKMlLdiuzyjHexG1zB.jpeg', '82', '1,83', 'castanhos', 'castanhos', '6', '2', '2', '5', '1', '3', NULL, '2018-09-23 01:19:24', '2018-09-23 01:19:24'),
(127, 'T\'challa', NULL, NULL, NULL, 'Wakanda', 'Pantera Negra', 'Vingadores, Pendragons', 'public/ciXHA1HR49RVVnlj5ibMdTAdCHwxMLi3NsAQsGPo.jpeg', '91', '1,83', 'castanhos', 'pretos', '5', '3', '2', '3', '3', '5', NULL, '2018-09-23 01:21:51', '2018-09-23 01:21:51'),
(128, 'Edward Brock', NULL, NULL, NULL, 'Nova York', 'Venon', 'Sexteto Sinistro', 'public/t76lxb75c3TYWoAs3mV3j3OpwPKLOCISQXn5kmMt.jpeg', '118', '1,91', 'azuis', 'castanhos', '2', '4', '3', '5', '1', '2', NULL, '2018-09-23 01:29:07', '2018-09-23 01:29:07'),
(129, 'Robert Drake', 'null', 'null', NULL, 'Fort Washington, NY', 'Homen de Gelo', 'X-men, X-Factory', 'public/VwAgYoHKITu39EmQormjwWEOloZ7R0qQ0ePp5yi6.jpeg', '66', '1,55', 'castanhos', 'castanhos', '2', '2', '3', '3', '5', '3', NULL, '2018-09-23 01:31:56', '2018-09-23 01:32:07'),
(130, 'Remy Etienne LeBeau', NULL, NULL, NULL, 'New Orleans', 'Gambit', 'X-men, Liga dos ladrões', 'public/OTBPGWDeBHUGwxF3WU6hs6FlPTXLgpLUVjHvXJ21.jpeg', '82', '1,86', 'verdes', 'castanhos', '2', '2', '2', '2', '2', '3', NULL, '2018-09-23 01:37:29', '2018-09-23 01:37:29'),
(132, 'Namor McKenzie', NULL, NULL, NULL, 'Antártida', NULL, 'Vingadores, Invasores, Esquadrão vitorioso, Defensores', 'public/8RoJQsmfcxAWrdKogC38VR25IX9R9aC2dp8eFMsJ.jpeg', '126', '1,90', 'azuis', 'pretos', '2', '6', '3', '6', '1', '3', NULL, '2018-09-23 01:41:34', '2018-09-23 01:41:34'),
(133, 'Loki Laufeyson', NULL, NULL, NULL, 'Jotunheim', NULL, NULL, 'public/NOg4Fl6hAsA8XJTneFvZih5luo1ZWgi3PZW0WST9.jpeg', '283', '1,96', 'verdes', 'pretos', '4', '5', '2', '5', '6', '1', NULL, '2018-09-23 01:45:08', '2018-09-23 01:45:08'),
(134, 'Raven Darkholme (Nome adotado)', 'null', 'null', NULL, 'desconhecido', 'Mística', 'null', 'public/4xtnJAqZdXaFZZCfrR5mVNRuJItD9D9r3Gl3sons.jpeg', '55', '1,77', 'amarelos', 'vermelhos', '3', '2', '2', '4', '1', '4', NULL, '2018-09-23 01:49:27', '2018-09-30 17:28:29'),
(135, 'Stephen Strange', NULL, NULL, NULL, 'Filadelfia, Pensilvânia', 'Dr. Estranho', NULL, 'public/khGog7WRKY3oPp4mens54Fi0DrQ6LDegIOrkbwSu.jpeg', '82', '1,89', 'acinzentados', 'pretos', '4', '2', '7', '3', '6', '3', NULL, '2018-09-24 02:40:31', '2018-09-24 02:40:31'),
(136, 'Susan Storm Richards', NULL, NULL, NULL, 'Glenville, NY', 'Mulher Invisível', 'Quarteto Fantástico, Vingadores', 'public/hgkgIA1iO98kVV7aZeZamXs1V76YgqJm3OFYn5lC.jpeg', '62', '1,67', 'azuis', 'loiros', '3', '2', '3', '5', '5', '3', NULL, '2018-09-24 02:45:10', '2018-09-24 02:45:10'),
(137, 'Kurt Wagner', NULL, NULL, NULL, 'Bavária, Alemanha', 'Noturno', NULL, 'public/FtWtZbr4WTmkvab8D0C019Sd0Uc6jjGgURjDkZ3r.jpeg', '73', '1,74', 'amarelos', 'indigo', '3', '2', '3', '2', '1', '3', NULL, '2018-09-24 02:52:32', '2018-09-24 02:52:32'),
(138, 'Medusalith Amaquelin Boltagon', NULL, NULL, NULL, 'Ilha de AtTilan, Oceano Atlântico', 'Medusa', NULL, 'public/KWkaOHhbcqBNy6eGXoRzrQnU1UMuG5e2NtImqMsY.jpeg', '59', '1,79', 'verdes', 'ruivos', '3', '3', '2', '3', '1', '2', NULL, '2018-09-24 03:00:00', '2018-09-24 03:00:00'),
(139, 'Daniel Raid-K\'ai', NULL, NULL, NULL, 'Não Revelado', 'Punho de Ferro', NULL, 'public/p8mX9BikmV98f8VSwESBBXz6rXcXEs8ozxBHaukl.jpeg', '80', '1,79', 'amarelos', 'loiros', '3', '2', '2', '3', '2', '6', NULL, '2018-09-24 03:03:18', '2018-09-24 03:03:18'),
(141, 'Katherine Pryde', NULL, NULL, NULL, 'Deerfield, Ilhinois', 'Lince Negra', 'SHIELD, X-Men', 'public/vSW9W9AFFThRf8j1AMLhBe3LxEXNiiwtmwn1bUoN.jpeg', '50', '1,67', 'castanhos', 'castanhos', '4', '2', '2', '3', '1', '5', NULL, '2018-09-24 03:10:29', '2018-09-24 03:10:29'),
(142, 'Curt Connors', NULL, NULL, NULL, 'Coral Gables, Califórnia', 'Lagarto', NULL, 'public/z5hqWwqJfioMEgr3xkO6kfLkXdQI3gKcuzkfB1P9.jpeg', '80', '1,81', 'vermelhos', 'castanhos', '5', '4', '2', '5', '1', '2', NULL, '2018-09-24 03:12:45', '2018-09-24 03:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(34, '2014_10_12_000000_create_users_table', 1),
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2018_08_27_013403_create_users_admin', 1),
(37, '2018_08_27_020015_create_admins_table', 1),
(38, '2018_08_29_102032_create_customer_table', 1),
(39, '2018_09_05_003339_users_add_column', 1),
(40, '2018_09_08_131053_create_products_table', 1),
(41, '2018_09_08_132340_create_eaglemoss_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estoque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `largura` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `altura` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Davi', 'Gasparino', 'dv.gasparino@gmail.com', '$2y$10$saVSSPgPuFHIvWBtlUhhI.ItLDMzll7MFh7Bkwr8Nv8uHHtW.HnVe', 'g1OIqi0FMad2FGfJ7nqcvhILg5WgM54G4RPidWu12fBiaU3RfdXxX0tql2XG', '2018-09-08 17:20:04', '2018-09-08 17:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `users_admin`
--

CREATE TABLE `users_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eaglemoss`
--
ALTER TABLE `eaglemoss`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_admin`
--
ALTER TABLE `users_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_admin_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `eaglemoss`
--
ALTER TABLE `eaglemoss`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_admin`
--
ALTER TABLE `users_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

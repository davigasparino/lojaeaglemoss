<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner_Home';

    protected $fillable = [
        'banner',
        'active',
        'link',
        'target'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eaglemoss extends Model
{
    protected $table = 'eaglemoss';

    protected $fillable = [
        'name',
        'codinomes',
        'descricao',
        'historia',
        'peso',
        'altura',
        'olhos',
        'cabelos',
        'filiacao',
        'local_de_nascimento',
        'inteligencia',
        'forca',
        'velocidade',
        'durabilidade',
        'projecao_de_energia',
        'combate',
        'foto',
        'pdf',
        'preco',
        'searchitens'
    ];
}

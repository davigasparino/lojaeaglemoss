<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Numbers extends Model
{
    protected $table = 'numbers';

    public $timestamps = false;

    protected $fillable = [
        'con',
        'data',
        'n1',
        'n2',
        'n3',
        'n4',
        'n5',
        'n6'
    ];
}

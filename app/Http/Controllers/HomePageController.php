<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eaglemoss;
use App\Banner;
use Illuminate\Support\Facades\Session;

class HomePageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * maneiras de usar o AGENT_ID
         *      $request->server('HTTP_USER_AGENT');
         *      $request->header('User-Agent');
        */
        $eaglemoss = Eaglemoss::orderBy('id', 'desc')->paginate(8);

        $banner = $this->getBanner();
        $this->carSession();
        return view('HomePage')->with('eaglemoss', $eaglemoss)->with('banner', $banner);
    }

    public function getBanner()
    {
        $obj = Banner::where('active', '1')->orderBy('id', 'desc')->paginate();
        return $obj;
    }

    public function carSession()
    {
        $word = $this->myEncrypt('Lorena*800');
        return Session::put(['Athena' => $word]);
    }

    public function myEncrypt($myword)
    {
        return md5($myword);
    }
}

<?php

namespace App\Http\Controllers;

use \App\Banner;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{

    public function index()
    {
        $obj = new Banner;
        return view('bannerHome.banner')->with('banner', $obj->orderBy('id', 'desc')->get());
    }

    public function createBanner(Request $req){

        $obj = new Banner;
        $obj->fill($req->all());

        if(isset($obj->banner) && $req->file('banner') !== null ){
            $obj->banner = $req->file('banner')->store('');
        }
        $obj->save();

        return response()->json($obj);
    }

    public function updateBanner(Request $req){
        $obj = Banner::find($req->input('id'));
        $deBanner = Banner::find($req->input('id'));
        $obj->fill($req->all());

        if(isset($obj->banner) && $req->file('banner') !== null ){
            Storage::delete($deBanner->banner);
            $obj->banner = $req->file('banner')->store('');
        }
        $obj->save();

        return response()->json($obj);
    }

    public function deleteBanner(Request $req){
        $obj = Banner::find($req->id);
        Storage::delete($obj->banner);
        $obj = Banner::find($req->id)->delete();

        return response()->json($obj);
    }
}

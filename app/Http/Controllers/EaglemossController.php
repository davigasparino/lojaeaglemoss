<?php

namespace App\Http\Controllers;

use App\Eaglemoss;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class EaglemossController extends Controller
{
    public function index(Request $req, $page = null)
    {
        $pg = 10;
        $search = $req->input('searchitens');
        $data = Eaglemoss::where('name','like','%'.$search.'%')
        ->orWhere('codinomes','like','%'.$search.'%')
        ->orWhere('filiacao','like', '%'.$search.'%')
        ->orWhere('id','like', '%'.$search.'%')
        ->orderBy('id', 'desc')
        ->paginate($pg);
        if($data->total()<1){
            $data = Eaglemoss::orderBy('id', 'desc')->paginate($pg);
        }
        if($page) {
            return view('/eaglemoss/eaglemoss')->with('eaglemoss', $data);
        }
        return view('/eaglemoss/index')->with('eaglemoss', $data);



    }


    public function eaglemoss(Request $req)
    {
        $pg = 10;
        $search = $req->input('search-filter');
        $inteligencia = $req->input('inteligencia');
        $forca = $req->input('forca');
        $velocidade = $req->input('velocidade');
        $durabilidade = $req->input('durabilidade');
        $projecao_de_energia = $req->input('projecao_de_energia');
        $combate = $req->input('combate');
        $erro = view('/eaglemoss/error');

        $data = Eaglemoss::orderBy('id', 'desc');
        if(!empty( $search )) {

            $data->where(function ($query) use ($search) {

                $query->where(function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%')
                        ->orWhere('codinomes', 'like', '%' . $search . '%')
                        ->orWhere('filiacao', 'like', '%' . $search . '%')
                        ->orWhere('id', 'like', '%' . $search . '%');
                });
            });

            if($data->paginate($pg)->total()<1)
                return $erro;

        }

        if($forca > 0) {
            $data = $data->where('forca', $forca);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }
        if($inteligencia > 0) {
            $data = $data->where('inteligencia', $inteligencia);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }
        if($velocidade > 0) {
            $data = $data->where('velocidade', $velocidade);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }
        if($durabilidade > 0) {
            $data = $data->where('durabilidade', $durabilidade);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }
        if($projecao_de_energia > 0) {
            $data = $data->where('projecao_de_energia', $projecao_de_energia);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }
        if($combate > 0) {
            $data = $data->where('combate', $combate);
            if($data->paginate($pg)->total()<1)
                return $erro;
        }

        $data = $data->paginate($pg);


        if($data->total()<1){
            $data = Eaglemoss::orderBy('id', 'desc')->paginate($pg);
        }


        return view('/eaglemoss/eaglemoss')->with('eaglemoss', $data);

    }

    public function create(Request $req)
    {
        $obj = new Eaglemoss;
        $obj->fill($req->all());
        if(isset($obj->foto)){
            $obj->foto = $req->file('foto')->store('');
        }
        if(isset($obj->pdf)){
            $obj->pdf = $req->file('pdf')->store('');
        }
        $obj->save();

        return response()->json($obj);
    }

    public function update(Request $req)
    {
        $obj = Eaglemoss::find($req->input('id'));
        $obj->fill($req->all());
        if(isset($obj->foto) && $req->file('foto') != null){
            $obj2 = Eaglemoss::find($req->id);
            Storage::delete($obj2->foto);
            $obj->foto = $req->file('foto')->store('');
        }
        if(isset($obj->pdf) && $req->file('pdf') != null){
            $obj2 = Eaglemoss::find($req->id);
            Storage::delete($obj2->pdf);
            $obj->pdf = $req->file('pdf')->store('');
        }

        $obj->save();

        return response()->json($obj);
    }

    public function deletar(Request $req)
    {
        $obj = Eaglemoss::find($req->id);
        Storage::delete($obj->foto);
        $obj = Eaglemoss::find($req->id)->delete();
        return response()->json($obj);
    }

    public function getTotal(){
        $obj = Eaglemoss::paginate(10);
        return response()->json($obj->total());
    }
}

<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::all();
        return view('customer/form')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $address = $req->input('address');
        $complement = $req->input('complement');
        $telefone = $req->input('telefone');
        $celular = $req->input('celular');
        $rg = $req->input('rg');
        $cpf = $req->input('cpf');
        $user_id = $req->input('user_id');

        $obj = new Customer;
        $obj->user_id = $user_id;
        $obj->rg = $rg;
        $obj->cpf = $cpf;
        $obj->address = $address;
        $obj->complement = $complement;
        $obj->telefone = $telefone;
        $obj->celular = $celular;
        $obj->save();

        return response()->json([
           'id' => $obj->id,
           'user_id' => $obj->user_id,
           'rg' => $obj->rg,
           'cpf' => $obj->cpf,
           'address' => $address,
           'complement' => $complement,
           'telefone' => $telefone,
           'celular' => $celular,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function formadd(Request $request)
    {
        return view('customer/form');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}

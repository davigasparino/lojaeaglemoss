<?php

namespace App\Http\Controllers;

use App\NumbersSimulator;
use Composer\Autoload\ClassLoader;
use Illuminate\Http\Request;
use App\Numbers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use function MongoDB\BSON\toJSON;
use phpDocumentor\Reflection\Types\Compound;

class NumbersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('numbers/index');
    }

    public function readCSV(Request $req){
        $result = explode('\n', hasToString($req->all()));

        foreach ($result as $key => $res){
            if(count(explode(',', $res)) == 8){
                $number = Numbers::where('con', explode(',', $res)[0])->get();
                if(!isset($number[0]['con'])){
                    $arr = str_replace( '\r', '', explode(',', $res));
                    $number = new Numbers([
                        'con' => $arr[0],
                        'data' => $arr[1],
                        'n1' => $arr[2],
                        'n2' => $arr[3],
                        'n3' => $arr[4],
                        'n4' => $arr[5],
                        'n5' => $arr[6],
                        'n6' => $arr[7]
                    ]);
                    $number->save();
                }
            }
        }
    }

    public function exportExcel(){
        $Numbers = Numbers::get();
        $data = [];
        $objPHPExcel = new \PHPExcel();

        $C = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

        $objPHPExcel->getActiveSheet()->setCellValue($C[0].'1', 'Concurso');
        $objPHPExcel->getActiveSheet()->setCellValue($C[1].'1', 'Data');
        $objPHPExcel->getActiveSheet()->setCellValue($C[2].'1', 'N1');
        $objPHPExcel->getActiveSheet()->setCellValue($C[3].'1', 'N2');
        $objPHPExcel->getActiveSheet()->setCellValue($C[4].'1', 'N3');
        $objPHPExcel->getActiveSheet()->setCellValue($C[5].'1', 'N4');
        $objPHPExcel->getActiveSheet()->setCellValue($C[6].'1', 'N5');
        $objPHPExcel->getActiveSheet()->setCellValue($C[7].'1', 'N6');


        foreach ($Numbers as $key => $val){
            $cont = (int) $key + 2;
            $objPHPExcel->getActiveSheet()->setCellValue($C[0].$cont, $val['con']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[1].$cont, $val['data']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[2].$cont, $val['n1']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[3].$cont, $val['n2']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[4].$cont, $val['n3']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[5].$cont, $val['n4']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[6].$cont, $val['n5']);
            $objPHPExcel->getActiveSheet()->setCellValue($C[7].$cont, $val['n6']);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numbers');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Numbers.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

    }

    public function luckNumbers(){
        $numbers = Numbers::get();

        $total = count($numbers);

        $allNumbers = [];
        $k = 0;
        for($j = 1; $j <= 60; $j++) {
            $var = 0;
            foreach ($numbers as $n => $x) {

                for ($i = 1; $i <= 6; $i++) {
                    if ($numbers[$n]['n' . $i] == $j) {
                        $var++;
                    }
                }

            }
            $k++;
            array_push($allNumbers, ['number' => $j, 'qtd' => $var]);
        }

        $allNumbers = $this->orderArr($allNumbers);

        return view('numbers.luck')->with('luckNumbers', $allNumbers)->with('total', $total);

    }

    public function oneMillion(){
        $res1 = '{"24":50391,"20":50355,"59":50340,"56":50340,"32":50302,"3":50286,"57":50262,"25":50260,"4":50260,"1":50255,"51":50251,"35":50217,"7":50217,"40":50195,"43":50153,"38":50134,"14":50112,"52":50110,"12":50105,"21":50102,"28":50097,"39":50092,"55":50047,"19":50044,"33":50015,"31":50015,"36":50010,"13":50005,"42":50000,"34":50000,"44":49994,"2":49992,"5":49969,"50":49966,"47":49960,"58":49926,"60":49926,"6":49925,"53":49918,"48":49906,"45":49875,"17":49869,"46":49862,"18":49855,"23":49848,"10":49845,"54":49842,"30":49840,"15":49835,"22":49832,"41":49828,"26":49813,"27":49797,"49":49785,"37":49752,"11":49748,"8":49656,"9":49633,"29":49553,"16":49478}';
        $res2 = '{"27":50436,"3":50391,"6":50321,"57":50297,"21":50284,"30":50273,"2":50270,"4":50232,"37":50170,"7":50152,"32":50148,"56":50139,"50":50136,"20":50124,"59":50122,"45":50113,"26":50110,"33":50109,"22":50097,"31":50082,"29":50081,"44":50078,"23":50077,"41":50066,"42":50042,"1":50024,"36":50021,"28":50012,"55":50008,"38":49999,"5":49994,"15":49994,"49":49980,"8":49973,"17":49969,"13":49968,"46":49956,"47":49955,"14":49949,"48":49940,"58":49935,"35":49933,"10":49927,"54":49910,"12":49908,"16":49900,"24":49863,"43":49859,"40":49851,"25":49841,"51":49779,"18":49779,"34":49768,"53":49746,"60":49745,"19":49739,"39":49719,"52":49659,"9":49617,"11":49430}';

        $arr = [] ;

        foreach (json_decode($res1) as $r2 => $r1){
            $arr[$r2] = $r1;
        }

        foreach (json_decode($res2) as $r2 => $r1){
            $arr[$r2] = $arr[$r2] + $r1;
        }

        //dd(array_sum($arr));

        $total = array_sum($arr);

        arsort($arr);
        return view('numbers.onemillion')->with('arr', $arr)->with('total', $total);
    }

    public function showAll()
    {
        $msena = Numbers::orderBy('id', 'desc')->paginate(8);
        return view('numbers.showAll')->with('megasena', $msena);
    }

    public function store(Request $req){
        $obj = new Numbers;
        $obj->fill($req->all());
        $obj->data = date('d/m/Y', strtotime($obj->data));
        $obj->save();
    }

    public function delete(Request $req){
        $obj = Numbers::find( (int) $req->all()['id']);
        $obj->delete();
    }

    public function generate(Request $req){
        set_time_limit(0);

        $tm = [];
        $min = 1;
        $max = 60;
        $total = (int)$req->all()['size'];

        for ($i = $min; $i <= $max; $i++) {
            $tm[$i] = 0;
        }


        for ($m = 0; $m < $total; $m++) {
            $t = [];
            for ($i = $min; $i <= $max; $i++) {
                $t[$i] = 0;
            }

            for ($i = 0; $i < $total; $i++) {
                $k = random_int($min, $max);
                $t[$k]++;
            }

            arsort($t);

            $i = 0;
            foreach ($t as $k => $v) {
                $tm[$k]++;
                $i++;
                if ($i == 6)
                    break;
            }

        }

        arsort($tm);

        $obj = new NumbersSimulator;
        $obj->content = json_encode($tm);
        $obj->size = $total;
        $obj->save();

        return view('numbers.generate')->with('arr', $tm)->with('total', $total);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index2()
    {
        $numbers = Numbers::get();

        $sortNumbers = $this->sortNumbers($numbers);

        $getCombination = $this->getCombination($numbers, array('21', '33', '55', '16', '7', '60'));

        dd($getCombination);

        dd($sortNumbers);


            dd('Número: '. $allNumbers[1]['number'] . ' Qtd.: ' . $allNumbers[1]['qtd']);
        exit();
    }

    public function getCombination(Request $req){

        $arr = [];

        foreach ($req->all() as $ar){
            if(isset($ar)){
                array_push($arr, $ar);
            }
        }

        if(count($arr) == 0){
            return '<div class="alert alert-danger" role="alert">nenhum resultado encontrado</div>';
        }

        $limit = count($arr);

        $numbers = Numbers::get();

        $arrRes = [];

        foreach ($numbers as $n => $x){
            $cont = 0;
            for($j = 0; $j < $limit; $j++) {
                for ($i = 1; $i <= 6; $i++) {
                    if($numbers[$n]['n'.$i] == $arr[$j]){
                        $cont ++;
                    }
                }
            }
            if($cont == $limit){
                array_push($arrRes, $numbers[$n]);
            }

        }

        return view('numbers.getCombination')->with('arr', $arrRes);
    }

    /**
     * @param $numbers
     * @return números mais recorentes (números da sorte)
     */


    public function orderArr($arr){

        usort($arr, function($a, $b){
            if($a['qtd'] == $b['qtd'])
                return 0;

            return $a['qtd'] > $b['qtd'] ? -1 : 1;
        });

        return $arr;
    }

    public function index3( )
    {
        $arr_caso_1 = array(3, 1, 2, 2, 4);
        $caso_1 = $this->checkOcurrences($arr_caso_1);

        /*
         neste caso (CASO 2) o resultado esperado segundo o PDF seria array(8, 4, 4, 1, 1, 1, 5, 5, 5, 5),
         mas não entendi o motivo da ordenação ser diferente, pois não identifiquei uma regra diferente para este caso
         Portanto mantive minha função do mesmo jeito.
        */
        $arr_caso_2 = array(8, 5, 5, 5, 5, 1, 1, 1, 4, 4);
        $caso_2 = $this->checkOcurrences($arr_caso_2);

        $arr_caso_3 = array(1, 2, 3, 7, 1, 8, 2);
        $caso_3 = $this->checkOcurrences($arr_caso_3);

        $exercicio_2_caso_1 = $this->getFibonacci(4);
        $exercicio_2_caso_2 = $this->getFibonacci(5);
        $exercicio_2_caso_3 = $this->getFibonacci(8);
        $exercicio_2_caso_4 = $this->getFibonacci(1);
        $exercicio_2_caso_5 = $this->getFibonacci(3);
        $exercicio_2_caso_6 = $this->getFibonacci(10);

        //dd($exercicio_2_caso_1, $exercicio_2_caso_2, $exercicio_2_caso_3, $exercicio_2_caso_4, $exercicio_2_caso_5, $exercicio_2_caso_6);
        $exercicio_3_caso_0 = 'aeiaaioooau';
        $exercicio_3_caso_1 = 'aeiaaioooaauuaeiou';
        $exercicio_3_caso_2 = 'aeiaaioooaa';
        $exercicio_3_caso_3 = 'uioieeeaouiiuaeeuuiuuauuauaeaeuauaeaaiuoiouaeuiuuoooaeeaioeieoeooaeuooae';
        $exercicio_3_caso_4 = 'iaaieeeoaueuaaaaieaooiiuiaueeoauiueuaeiaouiueoouaeeioeieoeeiiiouiaioiaeeaaaeaouiioiueuoieeoeoiuuuouiaoea
aeeeiueuiueiaieuoueoeooiuoooiooouuuoiuoeiuaouoeaaaiaeueaiaeouuaeioeoaeeuuaeouiauaiaoioueeiauuieouoe
uoiiooauoeaoieuieiaooaaieuoauueoeueeauuaaueeeeeoooouueoiaauooioioaeiiuaiueeoaeiuiaouieiueuae';
        $exercicio_3_caso_5 = 'ioeueooeuieoaioeoooiioieueoaiieaeaoeioiiaueueiououeiueeaaueeueaeoaaaouoeoieouaauooeuuoeauuaauaeoee
uiueeeuiieooeuaooeiaeueaaaaiooeaoiiiaaaooeiioaiiieieauaoeuiiueuioouuaoaioeiaiaaaaoeeaiuiaeoiiuauiiaeiuuaoa
eaaaaeoeueieoaaaooueioaauieieouoeouieaueuuaiiueoouioueuaaauaoeueuoouieuuouuoueoaaeuuouueieuouiooa
iuaoeuaeiaueuuieeiuaaeuiuuiuoiaiaeauuuaeeuuuuoieoieuaoiiuoeiaeaeeauoueaiuooiaoaiuoouoeeueeuaoeueiaiioi
ouueeaaoeoeauouuieeoaoioauieeeieeaaiuiaaeiaeueuouuaoaoiiaoeoaoeuieeouiiiiauauueaeouaeeeaoeaaaeouuue
oeoiueeoeiouaoeaaeeoaeaiiuouoaaeiuaiaeueuauaoauueuoeueueauuuueeeeuaouaaueaiouoeuooeiouoiiiueauaua
euaauuoeuoaeeouoouoeeeoieoaouiaaioiuoeuaaouuiioieoiiaueueuieouaiioeuaeoeieaoeiuooueeoeuueueioaoaauo
ooiiueueooeuouauuaiuiaoieeeeoouoeiaaaeieiooeouioeuooeeiauouueiuieaeaieeooaoeiuu';
        //dd($this->magicString($exercicio_3_caso_1), $this->magicString($exercicio_3_caso_2), $this->magicString($exercicio_3_caso_3), $this->magicString($exercicio_3_caso_4), $this->magicString($exercicio_3_caso_5));
//        dd($resp);


        // Caso 1
        $n = 3;
        $min = array(1,2,3);
        $max = array(2,4,5);
        $val = array(10,5,12);

        //caso 2
        $n = 3;
        $min = array(1,2,3);
        $max = array(2,5,4);
        $val = array(100,100,100);

        //caso 3
        $n = 30;
        $min = array(29,   9,  21,  8,  15, 11,  14,  5, 11, 5, 15,  3, 15, 29, 9, 33, 30, 11, 3, 29,  4,  1, 12,25,  3, 12,  3,  8,  8, 12);
        $max = array(40,  26,  31, 22,  23, 24,  22, 22, 30,25, 24, 10, 22, 35,20, 34, 38, 31,32, 35, 24, 38, 18,32, 39, 37, 26, 39, 11, 37);
        $val = array(787,219, 214,719, 102, 83, 321,300,832,29,577,905,335,254,20,351,564,969,11,267,531,892,825,99,107,131,640,483,194,502);

        //caso 4
        $n = 30;
        $min = array( 19,   4,   5, 19,   5, 10,  21, 13, 12,15,  8, 21, 20, 13,13, 15, 14, 20,10, 16, 25,  5, 17, 8, 22, 19,  2, 21, 23,  2);
        $max = array( 28,  23,   6, 33,   9, 13,  39, 18, 26,30,  9, 23, 21, 40,23, 15, 35, 39,18, 27, 40,  9, 20,20, 37, 37,  5, 29, 30,  8);
        $val = array(419, 680, 907, 582, 880, 438, 294, 678, 528, 261, 48, 131, 7, 65, 901, 914, 704, 522, 379, 8, 536, 190, 809, 453, 298, 112, 186, 184, 625, 960);

          dd($this->bigVal($n,$min,$max,$val));

    }

    public function bigVal($n, $min, $max, $val){

        $arr = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        for($i = 1; $i <= 40; $i ++){
            for($j = 0; $j < $n; $j++){

                if(!isset($min[$j])){
                    $min[$j] = 0;
                }
                if(!isset($max[$j])){
                    $max[$j] = 0;
                }

                if($i >= $min[$j] && $i <=$max[$j]){
                    $arr[$i] += $val[$j];
                }
            }
        }

        return max($arr);
    }

    public function luckyNumbers(){
        return Numbers::all()->groupBy('bola1');
    }

    //exercício 01
    public function checkOcurrences($arr)
    {
        $subArr = [];
        $newArr = [];

        //Criando sub conjuntos
        foreach (array_count_values($arr) as $key => $qtd){

            if($qtd > 1){
                for($j = 0; $j<$qtd; $j++){
                    array_push($subArr, $key);
                }
            }else{
                array_push($newArr, $key);
            }
        }

        //ordenando subconjuntos
        $newArr = $this->orderArr($newArr);
        $subArr = $this->orderArr($subArr);

        $arr = array_merge($newArr,$subArr);

        return $arr;
    }



    //exercício 02
    public function createArr($n)
    {
        $arr = [];
        for($i = 0; $i <= $n; $i++){
            array_push($arr, $i);
        }

        return $arr;
    }

    public function getFibonacci($n)
    {
        $arr = $this->createArr($n);
        $nArr = [];

        for($i = 0; $i < $n; $i++){
            if($i > 1) {
                $swap = $arr[$i];
                $arr[$i] = $arr[$i - 1] + $arr[$i - 2];
                $arr[$i + 1] = $swap;
            }
            array_push($nArr, $arr[$i]);
        }

        return $nArr;
    }

    public function magicString($string) {
        $arrayString = str_split(strtolower(str_replace(["\r","\n"," "], "", $string)));
        $sequence = ['a','e','i','o','u'];

        $sum = 0;
        $curSequence = 0;

        foreach($arrayString as $letter) {
            if ($letter == $sequence[$curSequence] || ($sum > 0 && isset($sequence[$curSequence+1]) && $sequence[$curSequence+1] == $letter)) {
                $sum++;
                if ($letter != $sequence[$curSequence]) {
                    $curSequence++;
                }
            }
        }
        if ($curSequence == count($sequence) - 1) {
            return $sum;
        }

        return 0;
    }



}

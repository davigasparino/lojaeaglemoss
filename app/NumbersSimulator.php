<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumbersSimulator extends Model
{
    protected $table = 'numbers_simulator';

    protected $fillable = [
        'content'
    ];
}

$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    getTotal();

    $('#dellEaglemoss').on('show.bs.modal', function(e){

        console.log($(e.relatedTarget).parents('.lineEagle:first').data());

        $('#dellEaglemoss .modal-body span.name').html($(e.relatedTarget).parents('.lineEagle:first').data('name'));
        $('#dellEaglemoss form input[name=name]').val($(e.relatedTarget).parents('.lineEagle:first').data('name'));
        $('#dellEaglemoss form input[name=id]').val($(e.relatedTarget).parents('.lineEagle:first').data('id'));

        var id = $(e.relatedTarget).parents('.lineEagle:first').data('id');

        $('#removeeagle').one('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var data = $('#dellEaglemoss form').serialize();

            $.ajax({
                type: 'POST',
                data: data,
                url: '/admin/eaglemoss/delete',
                success: function (response) {
                    $('#sairmodal').trigger('click');
                    $('#tr-eagle-' + id).fadeOut('Slow');
                    getTotal();
                    getToast('Eaglemoss Deletado com sucesso!', 5000, 'danger');
                },
                error: function () {
                    getToast('Erro ao tentar deletar Eaglemoss', 5000, 'danger');
                }
            });

        });

    });

    $('#viewfoto').on('show.bs.modal', function(e){
        var foto = $(e.relatedTarget).attr('data-foto');
        $('#viewfoto .modal-content').html('<img src="'+foto+'" style="width: 100%;" alt="" />');
    });

    $('#Edit-Eaglemoss').on('show.bs.modal', function(e){

        $('#Edit-Eaglemoss input[name=inteligencia]').val($(e.relatedTarget).parents('.lineEagle:first').data('inteligencia'));
        $('#Edit-Eaglemoss input[name=forca]').val($(e.relatedTarget).parents('.lineEagle:first').data('forca'));
        $('#Edit-Eaglemoss input[name=velocidade]').val($(e.relatedTarget).parents('.lineEagle:first').data('velocidade'));
        $('#Edit-Eaglemoss input[name=durabilidade]').val($(e.relatedTarget).parents('.lineEagle:first').data('durabilidade'));
        $('#Edit-Eaglemoss input[name=combate]').val($(e.relatedTarget).parents('.lineEagle:first').data('combate'));
        $('#Edit-Eaglemoss input[name=projecao_de_energia]').val($(e.relatedTarget).parents('.lineEagle:first').data('projecao_de_energia'));
        $('#Edit-Eaglemoss input[name=name]').val($(e.relatedTarget).parents('.lineEagle:first').data('name'));
        $('#Edit-Eaglemoss input[name=id]').val($(e.relatedTarget).parents('.lineEagle:first').data('id'));
        $('#Edit-Eaglemoss input[name=peso]').val($(e.relatedTarget).parents('.lineEagle:first').data('peso'));
        $('#Edit-Eaglemoss select[name=olhos]').val($(e.relatedTarget).parents('.lineEagle:first').data('olhos'));
        $('#Edit-Eaglemoss input[name=altura]').val($(e.relatedTarget).parents('.lineEagle:first').data('altura'));
        $('#Edit-Eaglemoss select[name=cabelos]').val($(e.relatedTarget).parents('.lineEagle:first').data('cabelos'));
        $('#Edit-Eaglemoss input[name=filiacao]').val($(e.relatedTarget).parents('.lineEagle:first').data('filiacao'));
        $('#Edit-Eaglemoss input[name=local_de_nascimento]').val($(e.relatedTarget).parents('.lineEagle:first').data('local_de_nascimento'));
        $('#Edit-Eaglemoss input[name=codinomes]').val($(e.relatedTarget).parents('.lineEagle:first').data('codinomes'));
        $('#Edit-Eaglemoss textarea[name=descricao]').val($(e.relatedTarget).parents('.lineEagle:first').data('descricao'));
        $('#Edit-Eaglemoss textarea[name=historia]').val($(e.relatedTarget).parents('.lineEagle:first').data('historia'));
        $('#Edit-Eaglemoss input[name=preco]').val($(e.relatedTarget).parents('.lineEagle:first').data('preco'));

        $('#Edit-Eaglemoss #add-picture-content').html('<img src="/uploads/'+$(e.relatedTarget).parents('.lineEagle:first').data('foto')+'" alt="" style="width: 100%;" />');

        $("input#fotoup").change(function(){
            readURL(this, '#add-picture-content img');
        });

        $('#btn-add-eaglemoss').one('click', function(e){
            e.preventDefault();

            var data = $('#form-eaglemoss')[0];
            var dados = new FormData(data);

            $.ajax({
                url:  '/admin/eaglemoss/update',
                type: 'POST',
                data: dados,
                processData: false,
                contentType: false,
                success: function(response){

                    $('#add-picture-content input, #add-picture-content label').remove();
                    $('#add-pdf-content input, #add-pdf-content label').remove();
                    $('button.close').click();

                    var html = '';

                    html += viewEagle(response);

                    $('#tr-eagle-'+response.id).html(html);
                    $('#tr-eagle-'+response.id+' #tr-eagle-'+response.id).insertAfter('#tr-eagle-'+response.id);
                    $('#tr-eagle-'+response.id).eq(0).remove();
                    getStar();
                    getToast('Eaglemoss foi Alterado com sucesso', 5000, 'success');

                },
                error: function(){
                    getToast('Náo foi possível alterar este Eaglemoss', 5000, 'danger');
                }
            });

        });
    });

   $('#Add-Eaglemoss').on('show.bs.modal', function(){

       $("input#fotoup").change(function(){
           $('#Add-Eaglemoss #add-picture-content').html('<img src="" alt="" style="width: 100%;" />');
           readURL(this, '#add-picture-content img');
       });

              $('#Add-Eaglemoss #btn-add-eaglemoss').one('click', function(e){
                  e.preventDefault();

                  var data = $('#Add-Eaglemoss #form-eaglemoss')[0];
                  var dados = new FormData(data);


                  $.ajax({
                      url:  '/admin/eaglemoss/create',
                      data: dados,
                      type: 'POST',
                      processData: false,
                      contentType: false,
                      success: function(response){
                          $('#add-picture-content input, #add-picture-content label').remove();
                          $('#add-pdf-content input, #add-pdf-content label').remove();
                          $('#Add-Eaglemoss button.close').click();

                          var html = '';

                          html += viewEagle(response);

                          $('#table-res').prepend(html);

                          $('#form-eaglemoss input').val('');

                          getStar();
                          getTotal();
                          getToast('Eaglemoss Inserido com sucesso!', 5000, 'success');

                      },
                      error: function(){
                          getToast('Erro ao tentar inserir Eaglemoss', 5000, 'danger');
                          return false;
                      }
                  });

              });

   });

   $('#Add-Banner').on('show.bs.modal', function(){

       $("#addbanner input#banner").change(function(){
           $('.img-banner').html('<img src="" alt="" style="width: 100%;" />');
           readURL(this, 'div.img-banner img');
       });

       $('#btn-add-eaglemoss').off('click').click(function(e){
           e.preventDefault();
           var data = $('#form-banner')[0];
           var dados = new FormData(data);

           $.ajax({
               type:    'post',
               url:     '/admin/banner/create',
               data:    dados,
               processData: false,
               contentType: false,
               success: function (response) {
                   $('#Add-Banner button.close').click();
                   getToast('Banner inserido com sucesso', 5000, 'success');
                   $('.ListResultBanenr').prepend('<div class="row">'+viewBanner(response)+'</div>');

               },
               error: function () {
                   $('#Add-Banner button.close').click()
                   getToast('Não foi possível inserir o banner', 5000, 'danger');
               }
           });
       });
   });

   $('#Edit-Banner').on('show.bs.modal', function(e){
       // console.log($(e.relatedTarget).parents('.listBanner:first').data());
       $('#Edit-Banner input[name=link]').val($(e.relatedTarget).parents('.listBanner:first').data('link'));
       $('#Edit-Banner input[name=id]').val($(e.relatedTarget).parents('.listBanner:first').data('id'));
       $('#Edit-Banner input[name=active]').parents('label').removeClass('active');

       $('.img-banner').html('<img src="/uploads/'+$(e.relatedTarget).parents('.listBanner:first').data('banner')+'" alt="" style=" max-width: 100%;">');

       $("#addbanner input#banner").change(function(){
           $('.img-banner').html('<img src="" alt="" style="width: 100%;" />');
           readURL(this, 'div.img-banner img');
       });    if($(e.relatedTarget).parents('.listBanner:first').data('active') == 1){
           $('#Edit-Banner input#active-on').parents('label').addClass('active');
       }else{
           $('#Edit-Banner input#active-off').parents('label').addClass('active');
       }

       $('#Edit-Banner input[name=target]').parents('label').removeClass('active');
       if($(e.relatedTarget).parents('.listBanner:first').data('target') == 1){
           $('#Edit-Banner input#blank-on').parents('label').addClass('active');
       }else{
           $('#Edit-Banner input#blank-off').parents('label').addClass('active');
       }

       $('#Edit-Banner #btn-add-eaglemoss').click(function(e){
           e.preventDefault();
           var data = $('#Edit-Banner #form-banner')[0];
           var dados = new FormData(data);

           $.ajax({
               type:    'post',
               url:     '/admin/banner/update',
               data:    dados,
               processData: false,
               contentType: false,
               success: function (response) {
                   console.log(response.id);
                   $('#Edit-Banner button.close').click();

                   getToast('Banner Alterado com sucesso', 5000, 'success');
                   $('#listBannerID'+response.id).parents('.row:first').html(viewBanner(response));
               },
               error: function () {
                   $('#Edit-Banner button.close').click()
                   getToast('Não foi possível alterar o banner', 5000, 'danger');
               }
           });
       });
   });

    $('#dellBanner').on('show.bs.modal', function(e){

       $('#dellBanner form input[name=id]').val($(e.relatedTarget).parents('.listBanner:first').data('id'));

        var id = $(e.relatedTarget).parents('.listBanner:first').data('id');

        $('#removebanner').one('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var data = $('#dellBanner form').serialize();

            $.ajax({
                type: 'POST',
                data: data,
                url: '/admin/banner/delete',
                success: function (response) {
                    $('#sairmodal').trigger('click');
                    $('#listBannerID'+ id).fadeOut('Slow');
                    getTotal();
                    getToast('Banner Deletado com sucesso!', 5000, 'danger');
                },
                error: function () {
                    getToast('Erro ao tentar deletar o banner', 5000, 'danger');
                }
            });

        });

    });

    $('#btn-register').click(function(e){
       e.preventDefault();

       var data = $('#form-register').serializeArray();


       $.ajax({
           type: 'post',
           url: '/register',
           data: data,
           success: function(response){
               $('#form-register').css('display','none');

               $.ajax({
                   url: '/customer/form',
                   success: function(html) {
                       $("#container-form").append(html);
                       $('#btn-customer').click(function(e){
                           e.preventDefault();

                           var data = $('#form-customer').serializeArray();


                           $.ajax({
                               type: 'post',
                               url: '/customer/create',
                               data: data,
                               success: function(response){
                                  window.location.href="/home";
                               },
                               error: function(){
                                   alert('Erro ao tentar inserir');
                               }
                           });

                       });
                   }
               });

           },
           error: function(){
               alert('Erro ao inserir registro.');
           }
       });

   });

   $('#search-itens').on('keyup', function(){
       $('#search-filter').val($(this).val());


       $('#filter-eaglemoss').submit();
   });

    $('#filter-eaglemoss .custom-range').on('change', function(e){
        $('#filter-eaglemoss').trigger('submit');
    });

   $('#filter-eaglemoss').on('submit', function(e){
       e.preventDefault();


       $.ajax({
           url: '/admin/eaglemoss',
           data: $('#filter-eaglemoss').serialize(),
           type: 'post',
           success: function(obj){
               //console.log(obj);
               $('.eagletable').html(obj);
               $('.total-search span').html('Total: ' + $('tbody').attr('data-total'));
               getStar();
           }
       })
   });


    $('#filter-inteligencia').parents('.form-row:first').find('.scalPower').html($('#filter-inteligencia').val());
    $('#filter-forca').parents('.form-row:first').find('.scalPower').html($('#filter-forca').val());
    $('#filter-velocidade').parents('.form-row:first').find('.scalPower').html($('#filter-velocidade').val());
    $('#filter-durabilidade').parents('.form-row:first').find('.scalPower').html($('#filter-durabilidade').val());
    $('#filter-projecao_de_energia').parents('.form-row:first').find('.scalPower').html($('#filter-projecao_de_energia').val());
    $('#filter-combate').parents('.form-row:first').find('.scalPower').html($('#filter-combate').val());



    $('#filter-inteligencia, input#inteligencia').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('#filter-forca, input#forca').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('#filter-velocidade, input#velocidade').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('#filter-durabilidade, input#durabilidade').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('#filter-projecao_de_energia, input#projecao_de_energia').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('#filter-combate, input#combate').on('change', function(){
        $(this).parents('.form-row:first').find('.scalPower').html($(this).val());
    });

    $('a.FilterBT').on('click', function(){
        $('.FormFilter').animate({
            height: 'toggle'
        },500, function(){

        });
    });

    $(document).on('click', 'button.eaglemossComparationBt' , function(e){
        e.preventDefault();
        var eaglemoss = $(this).parents('.lineEagle:first').data();

        enableCompare(eaglemoss);
    });

    $(document).on('click','.eaglemossPaginate ul.pagination a', function(e){
       e.preventDefault();
       var page = 'page='+$(this).attr('href').split('page=')[1], search = $(this).attr('href').split('/')[4];
       var rout;
       var searchval = $('#search-itens').val();

       console.log('fora do searchval = '+searchval);

       if(searchval){
           console.log('dentro do searchval');
            rout = page+'&searchitens='+searchval;
        }else{
           rout = page;
       }

       getPage(rout);
   });

   getStar();

    $(document).on('click', 'i.close', function(){
        $(this).parents('.eagle-compare-item').fadeOut('slow');
        $(this).parents('.eagle-compare-item').remove();
        var qtd = $('div.compare-eagle').data('qtd') - 1;
        $('div.compare-eagle').data('qtd', qtd);

    });


    /**
     *  início Numbers
     */


    $('#allNumbers').on('click', function(){

        var rout = '/admin/numbers/all';
        getpageNumbers(rout);


    });


    $('#searchtAllNumbers input').on('keyup', function(){
        //$('.searchtAllNumbers').append($(this).attr('data-id'));
        $.ajax({
            url: '/admin/numbers/get',
            data: $('#searchtAllNumbers form').serialize(),
            type: 'post',
            beforeSend: function(){
                $('.content-getallNumbers').html(getSpinners());
            },
            success: function(res){
                $('.content-getallNumbers').html(res);
            },
            error: function(){
                $('.content-getallNumbers').html('Error!');
                getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
            }
        })
    });

    $('#inserNumbers button').click(function(){
        var data = $('#inserNumbers form').serialize();

        $.ajax({
           url: '/admin/numbers/insert',
           data: data,
           type: 'post',
           success: function(response){
               $('.content-numbers').html(response);
               $('#allNumbers').click();
               getToast('Registro inserido com sucesso!', 5000, 'success');
           },
           beforeSend: function(){
              $('.content-numbers').html( getSpinners());
           },
           error: function(){
               getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
           }
        });
    });

    $('body').on('click', '.btn-number-delete', function(){

        var $This = $(this).parents('tr');
        var id = $This.find('th').attr('data-id');

        $('#removeNumber').click(function(){
            $('#dellNumbersModal .close').click();
            $This.fadeOut('slow');

            $.ajax({
                url: '/admin/numbers/delete/',
                data: {id : id},
                type: 'post',
                success: function(res){
                    getToast('O registro foi deletado com sucesso', 5000, 'success');
                },
                error: function(){
                    getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
                }
            });
        });

    });

    $('#generateCombination').click(function(){

       var Data = $('#simulator form').serialize();

        $.ajax({
          url: '/admin/numbers/generate',
          type: 'post',
          data: Data,
          beforeSend: function(){
              $('.content-generateNumbers .card-body').html( getSpinners());
          },
          success: function(res){
              getToast('Número gerado com sucesso', 5000, 'success');
              $('.content-generateNumbers .card-body').html(res);
          },
          error: function(){
              $('.content-generateNumbers .card-body').html('Error!');
              getToast('Houve algum erro', 5000, 'danger');
          }
       });
    });

    $('#onemillion').click(function(){
       $.ajax({
           url:'/admin/numbers/onemillion',
           type:'post',
           beforeSend: function(){
               $('.content-manyNumbers .card-body').html( getSpinners());
           },
           error: function () {
               $('.content-manyNumbers .card-body').html('Error!');
               getToast('Houve algum erro', 5000, 'danger');
           },
           success: function (res) {
               $('.content-manyNumbers .card-body').html(res);
           }
       });
    });

    $('#luckNumbers').click(function(){
       $.ajax({
           url : '/admin/numbers/luck',
           type: 'post',
           beforeSend: function(){
               $('.content-luckNumbers .card-body').html( getSpinners());
           },
           error: function () {
               $('.content-luckNumbers .card-body').html('Error!');
               getToast('Houve algum erro', 5000, 'danger');
           },
           success: function (res) {
               $('.content-luckNumbers .card-body').html(res);
           }
       })
    });

    $('#submitreadCSV').click(function (e) {
        e.preventDefault();
        var data = $('form.readCSV')[0];
        var dados = new FormData(data);

        $.ajax({
            url:'/admin/numbers/readcsv',
            data: dados,
            type: 'post',
            processData: false,
            dataType: false,
            beforeSend: function(){
                $('#allNumbersContent .card-body').html(getSpinners());
            },
            success: function(){
                getToast('Planilha importada com sucesso', 5000, 'success');
                $('#allNumbers').click();
            },
            error: function(){
                $('#allNumbersContent .card-body').html('Error!');
                getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
            }
        });
    });

    // $('#btexportExcel').click(function(){
    //    $.ajax({
    //       type: 'post',
    //       url: '/admin/numbers/export/excel',
    //       beforeSend: function(){
    //           $('#allNumbersContent .card-body').html(getSpinners());
    //       },
    //       success: function(res){
    //           getToast('Planilha exportada com sucesso', 5000, 'success');
    //           $('#allNumbersContent .card-body').html(res);
    //       },
    //       error: function(){
    //           $('#allNumbersContent .card-body').html('Error!');
    //           getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
    //       }
    //    });
    // });

}); /** Ready */

function getpageNumbers(rout){
    $.ajax({
        url: rout,
        type: 'post',
        beforeSend: function(){
            $('#allNumbersContent .card-body').html( getSpinners());
        },
        success: function(res){
            $('#allNumbersContent .card-body').html(res);
            $('#allNumbersContent .card-body .pagination li a').click(function(e){
                e.preventDefault();
                rout = '/admin/numbers/all?page='+$(this).text();
                getpageNumbers(rout);
            });
            $('#allNumbersContent .card-body .pagination li').addClass('page-item');
            $('#allNumbersContent .card-body .pagination li span, #allNumbersContent .card-body .pagination li a').addClass('page-link');
        },
        error: function(){
            $('#allNumbersContent .card-body').html('Error!');
            getToast('Houve algum erro, tente novamente mais tarde', 5000, 'danger');
        }
    });
}

function getSpinners(){
    var html = `<div class="d-flex justify-content-center p-5 m-5">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>`;
    return html;
}

function getPage(rout){
    var $rout = '/admin/eaglemoss/?'+rout;

    console.log('rota: '+ $rout);
    $.ajax({
        url: $rout,
        data: $('#filter-eaglemoss').serialize(),
        type: 'post',
        beforeSend: function(){
            $('.eagletable').html(getSpinners());
        },
        success: function(obj){
            window.history.pushState(new Date, false, $rout);
            $('.eagletable').html(obj);
            getStar();

            // location.hash = rout;
            $('.total-search span').html('Total: ' + $('tbody').attr('data-total'));
        }
    });
}

function viewBanner(response){

    var html = '';

    html += `
        <div class="listBanner" id="listBannerID`+response.id+`" data-banner="`+response.banner+`" data-target="`+response.target+`" data-active="`+response.active+`" data-link="`+response.link+`" data-id="`+response.id+`">
            <div class="col-sm-3">
                <img src="/uploads/`+response.banner+`" alt="" style="max-width: 100%"/>
            </div>
            <div class="col-sm-3">
                `+response.link+`
            </div>
            <div class="col-sm-2">
                `;

    if(response.target != 1){
        html += 'Mesma aba';
    }else{
        html += 'Nova aba';
    }

    html += `
            </div>
            <div class="col-sm-2">`;

    if(response.active == 1){
        html += 'Ativo';
    }else{
        html += 'Inativo';
    }

    html +=`        </div>
            <div class="col-sm-2">
                <button type="button"
                        class="btn btn-light"
                        data-toggle="modal"
                        data-target="#Edit-Banner"><i class="far fa-edit"></i>
                </button>
                <button
                        data-toggle="modal"
                        data-target="#dellBanner"
                        class="btn btn-light ">
                    <i class="far fa-trash-alt"></i>
                </button>
            </div>
        </div>
    `;

    return html;
}

function viewEagle(response) {
    console.log(response);
    var html = `<div class="row lineEagle p-0" id="tr-eagle-` + response.id + `"
             data-name="` + response.name + `"
             data-id="` + response.id + `"
             data-codinomes="` + response.codinomes + `"
             data-descricao="` + response.descricao + `"
             data-historia="` + response.historia + `"
             data-peso="` + response.peso + `"
             data-preco="` + response.preco + `"
             data-altura="` + response.altura + `"
             data-olhos="` + response.olhos + `"
             data-filiacao="` + response.filiacao + `"
             data-local_de_nascimento="` + response.local_de_nascimento + `"
             data-cabelos="` + response.cabelos + `"
             data-inteligencia="` + response.inteligencia + `"
             data-forca="` + response.forca + `"
             data-velocidade="` + response.velocidade + `"
             data-durabilidade="` + response.durabilidade + `"
             data-pdf="` + response.pdf + `"
             data-foto="` + response.foto + `"
             data-combate="` + response.combate + `"
             data-projecao_de_energia="` + response.projecao_de_energia + `">
             <div class="col-1">
                ` + response.id + `
            </div>
            <div class="col-md-3 col-11 text-center p-2">
                <a data-toggle="modal" data-foto="/uploads/` + response.foto + `" data-target="#viewfoto" style="cursor: -webkit-zoom-in;">
                    <img
                            src="/uploads/` + response.foto + `"
                            alt=""
                            style="max-width: 100%" />
                </a></div>
            <div class="col-md-3 col-6 p-2">
                <table>
                    <tbody>
                    <tr>
                        <td>` + response.name + `</td>
                    </tr>
                    <tr>
                        <td>`+ response.codinomes +`</td>
                    </tr>
                    <tr>
                        <td>`+ response.filiacao +`</td>
                    </tr>
                    <tr>
                        <td>`+ response.local_de_nascimento +`</td>
                    </tr>
                    <tr>
                        <td>Olhos: `+ response.olhos +`</td>
                    </tr>
                    <tr>
                        <td>Altura: `+ response.altura +`</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 col-6 p-2">
                <table class="text-nowrap text-right">
                    <tbody>
                    <tr>
                        <td>
                            Inteligência:
                        </td>
                        <td class="scal-power" data-power="`+ response.inteligencia +`"></td>
                    </tr>
                    <tr>
                        <td>
                            Força:
                        </td>
                        <td class="scal-power" data-power="`+ response.forca +`"></td>
                    </tr>
                    <tr>
                        <td>
                            Velocidade:
                        </td>
                        <td class="scal-power" data-power="`+ response.velocidade +`"></td>
                    </tr>
                    <tr>
                        <td>
                            Durabilidade:
                        </td>
                        <td class="scal-power" data-power="`+ response.durabilidade +`"></td>
                    </tr>
                    <tr>
                        <td>
                            Proj. Energia:
                        </td>
                        <td class="scal-power" data-power="`+ response.projecao_de_energia +`"></td>
                    </tr>
                    <tr>
                        <td>
                            Combate:
                        </td>
                        <td class="scal-power" data-power="`+ response.combate +`"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1 col-12 text-center p-2">
                <button type="button"
                        class="btn btn-light"
                        data-toggle="modal"
                        data-target="#Edit-Eaglemoss"><i class="far fa-edit"></i>
                </button>
                <button
                        data-toggle="modal"
                        data-target="#dellEaglemoss"
                        class="btn btn-light ">
                    <i class="far fa-trash-alt"></i>
                </button>
                <button type="button"
                        class="btn btn-light eaglemossComparationBt">
                    <i class="fab fa-wpforms"></i>
                </button>
            </div>
        </div>`;

    return html;
}

function viewEagleCompare(response){
    console.log(response.foto);
    var html = '';
    var score = response.inteligencia + response.forca + response.velocidade + response.durabilidade + response.projecao_de_energia + response.combate;

    html +='<div class="col-md-6 eagle-compare-item p-4" data-score="'+ score +'"><div class="nav compareNav"><i class="far fa-window-close right close"></i></div>';

    html +='<div class="row col-12"><div class="col-xl-6 col-12" id="div-eagle-'+response.id+'" style="text-align: center;">';
    var foto = '/uploads/'+response.foto;
    html +='<a data-toggle="modal" data-foto="'+foto+'" data-target="#viewfoto">';
    html +='<img src="'+foto+'" alt="" style="height: 200px;" /></a>';

    html +='</div>';

    html +='<div class="col-xl-6 col-12 text-right">';
    html +='<h3 class="text-center">'+response.name;
    if(response.codinomes != undefined){
        html += ' '+response.codinomes;
    }
    html +='</h3><table style="width: 100%;" class="text-nowrap"><tbody>';
    html +='<tr><td>Inteligência:</td><td class="scal-power text-left" data-power="'+response.inteligencia+'"></td></tr>';
    html +='<tr><td>Força:</td><td class="scal-power text-left text-nowrap" data-power="'+response.forca+'"></td></tr>';
    html +='<tr><td>Velocidade:</td><td class="scal-power text-left" data-power="'+response.velocidade+'"></td></tr>';
    html +='<tr><td>Durabilidade:</td><td class="scal-power text-left" data-power="'+response.durabilidade+'"></td></tr>';
    html +='<tr><td>Proj. Energia:</td><td class="scal-power text-left" data-power="'+response.projecao_de_energia+'"></td></tr>';
    html +='<tr><td>Combate:</td><td class="scal-power text-left" data-power="'+response.combate+'"></td></tr>';
    html +='</tbody></table>';
    html +='</div></div>';
    html +='</div>';

    console.log($('div.eagle-compare-item').data('score'));
    console.log($('div.eaglemossComparation').data('score'));

    return html;
}

function getStar(){
    $('td.scal-power').html('');
    $('td.scal-power').each(function(j) {
        var $star = $(this).attr('data-power');
        for(i=0; i<7; i++){
            if(i < $star){
                $(this).append('<i class="fas fa-circle"></i>');
            }else{
                $(this).append('<i class="far fa-circle"></i>');
            }
        }
    });

}

function getTotal(){
    $.ajax({
        url:'/admin/eaglemoss/total',
        success: function(response){
            $('tbody').attr('data-total', 'Total: ' + response);
            $('.total-search span').html($('tbody').attr('data-total'));
        }
    });
}

function enableCompare(data) {
    if($('div.compare-eagle').data('qtd') < 3) {
        var qtd = $('div.compare-eagle').data('qtd') + 1;
        console.log(qtd);
        $('div.compare-eagle').data('qtd', qtd);

        $('.eaglemossComparation').append(viewEagleCompare(data));

        getStar();
    }else{
        getToast('Fila cheia, remova pelo menos um dos selecionados.', 3000, 'danger');
    }

    if($('.eagle-compare-item').length == 2){
        $('.eagle-compare-item').removeClass('strongalot');
        if($('.eagle-compare-item:first').data('score') > $('.eagle-compare-item:last').data('score')){
            $('.eagle-compare-item:first').addClass('strongalot');
        }else if($('.eagle-compare-item:first').data('score') < $('.eagle-compare-item:last').data('score')){
            $('.eagle-compare-item:last').addClass('strongalot');
        }
    }

}

function getToast(msn, exec, status){
    $('.box-toast-all').html(`
        <div class="toast" role="alert" aria-live="assertive" data-delay="`+ exec +`" aria-atomic="true">
            <div class="alert alert-`+ status +`">
             `+ msn +`
            
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    `);
    $('.toast').toast('show');
}

function readURL(input, selector) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(selector).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


<?php

/*
 *
 *
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('customer/form', 'CustomerController@formadd');

Route::post('/customer/create', 'CustomerController@create');

Route::group(['middleware' => 'admin'], function(){

    Route::group(['middleware' => 'auth:admin'], function(){
        Route::get('/admin', 'AdminController@index')->name('admin');

        Route::get('/admin/eaglemoss', 'EaglemossController@index');
        Route::post('/admin/eaglemoss', 'EaglemossController@eaglemoss')->name('eaglemoss');

        Route::get('admin/banner', 'BannerController@index')->name('banner');
        Route::post('/admin/banner/create', 'BannerController@createBanner');
        Route::post('/admin/banner/update', 'BannerController@updateBanner');
        Route::post('/admin/banner/delete', 'BannerController@deleteBanner');

        Route::post('/admin/eaglemoss/searchitens/{searchitens}/null', 'EaglemossController@searchitens');
        Route::get('/admin/eaglemoss/searchitens/null', 'EaglemossController@searchitens');
        Route::get('/admin/eaglemoss/form', function(){
            return view('/eaglemoss/form');
        });

        Route::post('/admin/eaglemoss/create', 'EaglemossController@create');
        Route::post('/admin/eaglemoss/update', 'EaglemossController@update');
        Route::post('/admin/eaglemoss/delete', 'EaglemossController@deletar');
        Route::get('/admin/eaglemoss/total', 'EaglemossController@getTotal');

        Route::get('/admin/products', function(){
            return 'Products';
        });

        Route::get('/admin/numbers', 'NumbersController@index')->name('numbers');
        Route::post('/admin/numbers/all', 'NumbersController@showAll');
        Route::post('/admin/numbers/get', 'NumbersController@getCombination');
        Route::post('/admin/numbers/insert', 'NumbersController@store');
        Route::post('/admin/numbers/delete', 'NumbersController@delete');
        Route::post('/admin/numbers/generate', 'NumbersController@generate');
        Route::post('/admin/numbers/onemillion', 'NumbersController@onemillion');
        Route::post('/admin/numbers/luck', 'NumbersController@luckNumbers');
        Route::post('/admin/numbers/readcsv', 'NumbersController@readCSV');
        Route::get('/admin/numbers/export/excel', 'NumbersController@exportExcel');
    });



    Route::get('/admin/login', 'AdminController@login');
    Route::get('/admin/logout', 'AdminController@logout');
    Route::post('/admin/login', 'AdminController@postLogin');
});
